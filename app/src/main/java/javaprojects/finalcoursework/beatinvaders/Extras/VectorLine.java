package javaprojects.finalcoursework.beatinvaders.Extras;

/**
 * Vector class used for calculating bullet trajectories
 */
public class VectorLine {

    public Point P1;
    public Point P2;

    public double XVec;
    public double YVec;

    public double Size;

    /**
     * Takes in an origin point and constructs vector details from X and Y vectors
     * @param P1
     * @param XVec
     * @param YVec
     */
    public VectorLine(Point P1, double XVec, double YVec) {
        P2 = new Point(P1.X + XVec, P1.Y + YVec);

        this.P1 = new Point(P1);
        this.XVec = XVec;
        this.YVec = YVec;

        Size = Math.sqrt((Math.pow(XVec, 2) + Math.pow(YVec, 2)));
        Size = ((int) (Size * 10) + 0.5) / 10; //Round to 1dp
        FlipX();
    }

    /**
     * Takes two points and constructs a vector
     * @param P1
     * @param P2
     */
    public VectorLine(Point P1, Point P2) {
        Point DPx = new Point(P2.X - P1.X, P2.Y - P1.Y);
        XVec = DPx.X;
        YVec = DPx.Y;
        this.P1 = new Point(P1);
        this.P2 = new Point(P2);

        Size = Math.sqrt((Math.pow(XVec, 2) + Math.pow(YVec, 2)));
        Size = ((int) (Size * 10) + 0.5) / 10; //Round to 1dp
        FlipX();
    }

    /**
     * Increments the vector by an amount
     * @param Amount
     */
    public void Increment(double Amount) {
        Point PDx = new Point(XVec * Amount, YVec * Amount);
        P1 = new Point(P1.X - PDx.X, P1.Y - PDx.Y);
        P2 = new Point(P2.X - PDx.X, P2.Y - PDx.Y);
    }

    /**
     * Flips X axis, used due to canvas y orientation
     */
    public void FlipX() {
        XVec = -XVec;
    }

    public String toString() {
        String Construct = "";
        Construct += "P1: " + P1.toString() + " P2: " + P2.toString() + " Vector: " + XVec + ", " + YVec;
        return Construct;
    }
}

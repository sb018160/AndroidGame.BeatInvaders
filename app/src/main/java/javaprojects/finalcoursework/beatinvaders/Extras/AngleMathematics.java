package javaprojects.finalcoursework.beatinvaders.Extras;

/**
 * Library for rotating points and vectors
 */
public class AngleMathematics {

    /**
     * Rotates a point around its origin by an angle then returns the newly rotated point
     * @param Origin point
     * @param ToBeRotated point
     * @param Angle in degrees
     * @return new rotated point
     */
    static public Point RotateAroundOrigin(Point Origin, Point ToBeRotated, double Angle){
        Angle = Angle  * (Math.PI/180);
        double RotatedX = Math.cos(Angle) * (ToBeRotated.X - Origin.X) - Math.sin(Angle) * (ToBeRotated.Y-Origin.Y) + Origin.X;
        double RotatedY = Math.sin(Angle) * (ToBeRotated.X - Origin.X) + Math.cos(Angle) * (ToBeRotated.Y-Origin.Y) + Origin.Y;

        return new Point(RotatedX,RotatedY);
    }

    /**
     * Rotates a vector by an angle, treating p1 as origin
     * @param ToRotate vector
     * @param Angle in degrees
     * @return newly rotated vector
     */
    static public VectorLine RotateVector(VectorLine ToRotate, double Angle){
        Point P2 = RotateAroundOrigin(ToRotate.P1,ToRotate.P2, Angle);
        return new VectorLine(ToRotate.P1,P2);
    }

    /**
     * Calculates the angle between two points
     * @param P1 point
     * @param P2 point
     * @return angle between points
     */
    static public double CalculateAngle(Point P1, Point P2){
        double Angle = Math.toDegrees(Math.atan2(P2.X - P1.X, P2.Y - P1.Y));
        Angle = Angle + Math.ceil( -Angle / 360 ) * 360;
        return Angle;
    }

    /**
     * Calculates vector between two points and a given speed
     * @param location point 1
     * @param goalPoint point 2
     * @param speed speed for vector
     * @return generated vector
     */
    static public VectorLine CalculateVector(Point location, Point goalPoint, double speed) {
        VectorLine VL;
        double Angle = AngleMathematics.CalculateAngle(location,goalPoint);
        goalPoint = new Point(location);
        goalPoint = new Point(goalPoint.X, goalPoint.Y + speed);
        VL = new VectorLine(location, goalPoint);
        VL = AngleMathematics.RotateVector(VL,Angle);
        return VL;
    }
}

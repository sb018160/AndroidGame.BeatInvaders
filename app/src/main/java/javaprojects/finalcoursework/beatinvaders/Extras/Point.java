package javaprojects.finalcoursework.beatinvaders.Extras;

/**
 * Basic point class holding a double and rounded int value for x and y
 */
public class Point {
    public double X,Y;
    public int Xint,Yint;

    /**
     * Takes in X and Y points to save and calculates rounded int
     * @param Xnew
     * @param Ynew
     */
    public Point(double Xnew, double Ynew){
        X = Xnew;
        Y = Ynew;
        ConvertInt();
    }

    /**
     * Converts points to int
     */
    private void ConvertInt(){
        Xint = (int) (X+0.5);
        Yint = (int) (Y+0.5);
    }

    /**
     * Copies existing point
     * @param P to copy
     */
    public Point(Point P){
        X = P.X;
        Y = P.Y;
        ConvertInt();
    }
}

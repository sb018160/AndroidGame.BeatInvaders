package javaprojects.finalcoursework.beatinvaders.Game.Buffer;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;

/**
 * Buffered version of a bullet, does not hold location or vector so takes up less memory
 */
public class BufferedBullet {

    public int SpawnTime;
    public int Type;
    public Point SpawnPoint;

    public int Speed;
    public int Size;

    public int[] TypeModifiers;

    public boolean Used = false;

    /**
     * Takes in string from the bullet file
     * @param ObstacleConstruct String construct from file
     */
    public BufferedBullet(String ObstacleConstruct) {
        SplitConstruct(ObstacleConstruct);
    }

    /**
     * Creates bullet from existing details
     * @param SpawnTime
     * @param Type
     * @param SpawnPoint
     * @param Speed
     * @param Size
     * @param TypeModifiers
     */
    public BufferedBullet(int SpawnTime,int Type, Point SpawnPoint, int Speed, int Size, int[] TypeModifiers){
        this.SpawnTime = SpawnTime;
        this.Type = Type;
        this.SpawnPoint = SpawnPoint;
        this.Speed = Speed;
        this.Size = Size;
        this.TypeModifiers = TypeModifiers;
    }

    /**
     * Splits construct to generate details
     * @param Construct
     */
    private void SplitConstruct(String Construct){
        String[] BulletDetails = Construct.split(",");
        SpawnTime = Integer.parseInt(BulletDetails[0]);
        int StartX = Integer.parseInt(BulletDetails[1]);
        int StartY = Integer.parseInt(BulletDetails[2]);
        Type = Integer.parseInt(BulletDetails[3]);
        SpawnPoint = new Point(StartX,StartY);
        Speed = Integer.parseInt(BulletDetails[4]);
        Size = Integer.parseInt(BulletDetails[5]);
        int ExtraModifiers = BulletDetails.length;
        if(ExtraModifiers >= 7){
            TypeModifiers = new int[ExtraModifiers - 6];
            for(int i = 0; i < TypeModifiers.length;i++){
                TypeModifiers[i] = Integer.parseInt(BulletDetails[6+i]);
            }
        }
    }
}

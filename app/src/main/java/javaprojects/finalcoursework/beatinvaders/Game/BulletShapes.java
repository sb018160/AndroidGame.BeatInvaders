package javaprojects.finalcoursework.beatinvaders.Game;


import java.util.ArrayList;
import java.util.Random;

import javaprojects.finalcoursework.beatinvaders.Extras.AngleMathematics;
import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Extras.VectorLine;
import javaprojects.finalcoursework.beatinvaders.Game.Objects.Bullet;

/**
 * Libary for making defined shapes of bullets
 */
public class BulletShapes {

    /**
     * Creates a star/circle of bullets, angle between the bullets is equal
     * @param NumberOfBullets for the shape
     * @param XLoc of centre
     * @param YLoc of centre
     * @param Speed of all bullets
     * @param Size of all bullets
     * @return list of bullets in the shape
     */
    static public ArrayList<Bullet> Star(int NumberOfBullets, double XLoc, double YLoc, double Speed, double Size){
        ArrayList<Bullet> StarBullets = new ArrayList<>();
        double DegreesPerShot = 360/(float)NumberOfBullets;
        Point OriginPoint = new Point(XLoc,YLoc);
        for(int Counter = 0; Counter < NumberOfBullets; Counter++){
            double CurrentDegrees = DegreesPerShot * Counter;
            VectorLine VLine = new VectorLine(OriginPoint, 0,Speed);
            VLine = AngleMathematics.RotateVector(VLine,CurrentDegrees);
            StarBullets.add(new Bullet(VLine,Size));
        }
        return StarBullets;
    }

    /**
     * Rotates list of bullets by a set degree
     * @param BulletsList to rotate
     * @param Degrees to rotate by
     * @return list rotated
     */
    static public ArrayList<Bullet> Rotate(ArrayList<Bullet> BulletsList, double Degrees){
        for(Bullet B: BulletsList){
            VectorLine VL = B.VL;
            VL = AngleMathematics.RotateVector(VL, Degrees);
            B.SetVelocity(VL.XVec,VL.YVec);
        }
        return BulletsList;
    }

    /**
     * Returns flower of bullets
     * @param Layers of the flower
     * @param Spread angle between each layer
     * @param NumberOfBullets for the shape
     * @param XLoc of centre
     * @param YLoc of centre
     * @param Speed of all bullets
     * @param Size of all bullets
     * @return list of bullets in the shape
     */
    static public ArrayList<Bullet> Flower(int NumberOfBullets, int Layers, double Spread, double XLoc, double YLoc, double Speed, double Size){
        ArrayList<Bullet> FlowerBullets = new ArrayList<>();
        ArrayList<Bullet> CurrentLayer = new ArrayList<>();
        for(int N = 0; N < Layers; N++){
            CurrentLayer = Star(NumberOfBullets,XLoc,YLoc,Speed+(2*N),Size);
            if(N>0){
                CurrentLayer = Rotate(CurrentLayer,(2*N)*Spread);
            }
            FlowerBullets.addAll(CurrentLayer);
        }
        return FlowerBullets;
    }

    /**
     * Returns spread out bullets.
     * @param NumberOfBullets for shape
     * @param SpreadVariance max for random angle that each bullet will be given
     * @param SpeedVariation max for random speed each bullet will be given
     * @param XLoc of centre
     * @param YLoc of centre
     * @param Speed of bullets
     * @param Size of bullets
     * @return list of bullets in the shape
     */
    static public ArrayList<Bullet> RandomSpread(int NumberOfBullets, int SpreadVariance, int SpeedVariation, double XLoc, double YLoc, double Speed, double Size){
     ArrayList<Bullet> SpreadBullets = new ArrayList<>();
     Random Rng = new Random();
     Point OriginPoint = new Point(XLoc,YLoc);
     for(int i = 0; i < NumberOfBullets; i++){
         double AngleVariance = 0;
         if(SpreadVariance!=0) {
             AngleVariance = -SpreadVariance + Rng.nextInt(SpreadVariance * 2);
         }
         double SpeedVariance = 0;
         if(SpeedVariation != 0) {
             SpeedVariance  = -SpeedVariation + Rng.nextInt(SpeedVariation * 2);
         }
         VectorLine VL = new VectorLine(OriginPoint, 0, Speed+SpeedVariance);
         VL = AngleMathematics.RotateVector(VL,AngleVariance);
         Bullet B = new Bullet(VL,Size);
         SpreadBullets.add(B);
     }
     return SpreadBullets;
    }

}

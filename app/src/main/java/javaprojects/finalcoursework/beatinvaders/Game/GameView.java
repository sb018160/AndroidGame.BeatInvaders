package javaprojects.finalcoursework.beatinvaders.Game;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;

/**
 * Canvas for game, passes touch information to reference of game
 */
public class GameView extends View {

    Game TheGame;

    double Height, Width;

    boolean WaitingForDraw = false;


    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Sets up game reference and creates onTouchListeners
     * @param theGame
     */
    public void SetUp(Game theGame) {
        TheGame = theGame;

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        TheGame.UpdateMouse(0, new Point(e.getX(), e.getY()));
                        return true;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        TheGame.UpdateMouse(1, new Point(e.getX(), e.getY()));
                        return true;
                    }
                    case MotionEvent.ACTION_UP: {
                        TheGame.UpdateMouse(2, new Point(0, 0));
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        Height = getMeasuredHeight();
        Width = getMeasuredWidth();

        TheGame.SetSize(Width,Height);
        invalidate();
    }

    /**
     * Calls game to update
     */
    public void UpdateGame() {
        TheGame.Update();
    }

    /**
     * Draws game is the game is not currently updating
     * @param canvas
     */
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Height = getMeasuredHeight();
        Width = getMeasuredWidth();

        TheGame.SetSize(Width,Height);
        TheGame.Draw(canvas);
        WaitingForDraw = false;
    }
}

package javaprojects.finalcoursework.beatinvaders.Game.Buffer;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javaprojects.finalcoursework.beatinvaders.Game.Objects.Bullet;
import javaprojects.finalcoursework.beatinvaders.Game.Objects.Enemy;
import javaprojects.finalcoursework.beatinvaders.Game.Objects.Spawner;
import javaprojects.finalcoursework.beatinvaders.Song.Song;

/**
 * Class for managing all buffers of enemies/bullets/spawners and spawning them when required by gametime.
 */
public class BufferManager {
    ArrayList<BufferedEnemy> EnemyBuffer;
    ArrayList<BufferedBullet> BulletBuffer;
    ArrayList<Spawner> SpawnerBuffer;


    ArrayList<Enemy> Enemies;
    ArrayList<Bullet> LoadedBullets;
    ArrayList<Spawner> Spawners;

    public ArrayList<Bullet> ReadyToSpawnBullets;
    public ArrayList<Spawner> ReadyToSpawnSpawners;

    Context C;

    /**
     * Takes references to the game object arrays to spawn objects within game
     * @param GameEnemies
     * @param GameBullets
     * @param GameSpawner
     * @param ToBuffer
     * @param C
     */
    public BufferManager(ArrayList<Enemy> GameEnemies, ArrayList<Bullet> GameBullets, ArrayList<Spawner> GameSpawner, Song ToBuffer, Context C) {
        this.C = C;
        Enemies = GameEnemies;
        LoadedBullets = GameBullets;
        Spawners = GameSpawner;
        SetUpBuffers(ToBuffer);
    }

    /**
     * Checks buffer according to the game timer
     * @param GameTimer
     */
    public void CheckBuffer(double GameTimer) {
        ReadyToSpawnBullets = new ArrayList<>();
        ReadyToSpawnSpawners = new ArrayList<>();
        CheckEnemyBuffer(GameTimer);
        CheckBulletBuffer(GameTimer);
        CheckSpawnerBuffer(GameTimer);
    }

    /**
     * Checks enemy buffer and deletes any used buffers
     * @param GameTimer
     */
    private void CheckEnemyBuffer(double GameTimer) {
        ArrayList<Integer> UsedBuffers = new ArrayList<>();
        int i = 0;
        int DeletedNo = 0;

        for (BufferedEnemy BO : EnemyBuffer) {
            if (BO.SpawnTime <= GameTimer) {
                Enemy E = new Enemy(BO);
                E.Update(GameTimer - BO.SpawnTime);
                Enemies.add(E);
            }
            if (BO.Used) {
                UsedBuffers.add(i);
            } else if (i > 0) {
                break;
            }
            i++;
        }
        for (Integer I : UsedBuffers) {
            EnemyBuffer.remove(I - DeletedNo);
            DeletedNo++;
        }
    }

    /**
     * Checks bullet buffer and sees if any bullets need to be added to the game
     * @param GameTimer
     */
    private void CheckBulletBuffer(double GameTimer) {
        ArrayList<Integer> UsedBuffers = new ArrayList<>();
        int i = 0;
        int DeletedNo = 0;

        for (BufferedBullet BB : BulletBuffer) {
            if (BB.SpawnTime <= GameTimer) {
                ArrayList<Bullet> NewBullets = Bullet.CreateBullets(BB);
                for (Bullet B : NewBullets) {
                    B.Update(GameTimer - BB.SpawnTime);
                }
                ReadyToSpawnBullets.addAll(NewBullets);
            }
            if (BB.Used) {
                UsedBuffers.add(i);
            } else if (i > 0) {
                break;
            }
            i++;
        }
        for (Integer I : UsedBuffers) {
            BulletBuffer.remove(I - DeletedNo);
            DeletedNo++;
        }
    }

    /**
     * Checks spawner buffer and checks if any spawners need to be added,
     * Also checks if enough time had passed for the spawner to spawn it's bullets and add's those.
     * @param GameTimer
     */
    private void CheckSpawnerBuffer(double GameTimer) {
        ArrayList<Integer> UsedBuffers = new ArrayList<>();
        int i = 0;
        int DeletedNo = 0;

        for (Spawner S : SpawnerBuffer) {
            if (S.SpawnTime <= GameTimer) {
                S.Update(GameTimer - S.SpawnTime);
                if(S.ReadyToSpawn){
                    ReadyToSpawnBullets.addAll(S.SpawnShots());
                }
                ReadyToSpawnSpawners.add(new Spawner(S));
                S.Dead = true;
            }
            if (S.Dead) {
                UsedBuffers.add(i);
            } else if (i > 0) {
                break;
            }
            i++;
        }
        for (Integer I : UsedBuffers) {
            SpawnerBuffer.remove(I - DeletedNo);
            DeletedNo++;
        }
    }

    /**
     * Fills buffers with information from the enemy, bullet and spawner files.
     * @param ToBuffer
     */
    private void SetUpBuffers(Song ToBuffer) {
        EnemyBuffer = new ArrayList<>();
        BulletBuffer = new ArrayList<>();
        SpawnerBuffer = new ArrayList<>();

        String BufferList = ReadFile(ToBuffer, "EnemiesFile.txt", C);

        String[] SplitBuffer = BufferList.split(";");
        if (SplitBuffer[0] != "") {
            for (int C = 0; C < SplitBuffer.length; C++) {
                EnemyBuffer.add(new BufferedEnemy(SplitBuffer[C]));
            }
        }

        BufferList = ReadFile(ToBuffer, "BulletsFile.txt", C);

        SplitBuffer = BufferList.split(";");
        if (SplitBuffer[0] != "") {
            for (int C = 0; C < SplitBuffer.length; C++) {
                BulletBuffer.add(new BufferedBullet(SplitBuffer[C]));
            }
        }

        BufferList = ReadFile(ToBuffer, "SpawnerFile.txt", C);

        SplitBuffer = BufferList.split(";");
        if (SplitBuffer[0] != "") {
            for (int C = 0; C < SplitBuffer.length; C++) {
                SpawnerBuffer.add(new Spawner(SplitBuffer[C]));
            }
        }
    }

    /**
     * Reads from level directory for the requested file.
     * @param Leveldetails
     * @param FileName
     * @param C
     * @return
     */
    private static String ReadFile(Song Leveldetails, String FileName, Context C) {
        String ans = "";
        BufferedReader br = null;
        try {
            File Directory = Environment.getExternalStorageDirectory();
            Directory = new File(Directory, "Beat Invaders");
            Directory = new File(Directory, "Songs");
            Directory = new File(Directory, Leveldetails.Title);
            Directory = new File(Directory, Leveldetails.LevelName);
            Directory = new File(Directory, FileName);
            FileReader FR = new FileReader(Directory);
            br = new BufferedReader(FR);
            String line;
            while ((line = br.readLine()) != null) {
                line = line.replace("\t", " ");                // replace tabs with spaces
                if (line.length() > 0) {                        // if not empty string
                    if (ans.length() > 0) ans = ans + ";";    // add to answer
                    ans = ans + line;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return ans;
    }
}

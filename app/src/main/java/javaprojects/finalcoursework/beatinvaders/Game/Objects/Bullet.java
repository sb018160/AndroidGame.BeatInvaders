package javaprojects.finalcoursework.beatinvaders.Game.Objects;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.location.Location;

import javaprojects.finalcoursework.beatinvaders.Extras.AngleMathematics;
import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Extras.VectorLine;
import javaprojects.finalcoursework.beatinvaders.Game.Buffer.BufferedBullet;
import javaprojects.finalcoursework.beatinvaders.Game.BulletShapes;

import java.util.ArrayList;

/**
 * Bullet object in game
 */
public class Bullet {

    public VectorLine VL;
    public double XOffset, YOffset;

    public double Rad;

    public int XGrid;
    public int YGrid;

    public boolean Dead = false;

    /**
     * Constructed from an X and Y location along with a X and Y vector and size
     * @param XL XLocation
     * @param YL YLocation
     * @param XV XVector
     * @param YV YVector
     * @param Size of bullet
     */
    public Bullet(double XL, double YL, double XV, double YV, double Size){
        VL = new VectorLine(new Point(XL,YL), XV, YV);

        XOffset = 0;
        YOffset = 0;

        Rad = Size;
        CheckGrid();
    }

    /**
     * Constructed from an existing vector and size
     * @param VL vector for bullet
     * @param Size of bullet
     */
    public Bullet(VectorLine VL, double Size){
        this.VL = VL;

        XOffset = 0;
        YOffset = 0;

        Rad = Size;
    }

    /**
     * Creates bullet from a buffered bullet
     * @param BB buffered bullet to create
     * @return list of bullets or a singular bullet based on BB parameters
     */
    public static ArrayList<Bullet> CreateBullets(BufferedBullet BB) {
     ArrayList<Bullet> AllBullets = new ArrayList<>();

     switch(BB.Type){
         case 0: { //Normal Bullet
             Bullet B = new Bullet(BB.SpawnPoint.X,BB.SpawnPoint.Y,0,BB.Speed,BB.Size);
             VectorLine VL = AngleMathematics.RotateVector(B.VL, BB.TypeModifiers[0]);
             B.SetVelocity(VL.XVec,VL.YVec);
             AllBullets.add(B);
             break;
         }
         case 1:{ //Star
             AllBullets = BulletShapes.Star(BB.TypeModifiers[1],BB.SpawnPoint.X,BB.SpawnPoint.Y,BB.Speed,BB.Size);
             if(BB.TypeModifiers[0] != 0) {
                 AllBullets = BulletShapes.Rotate(AllBullets, BB.TypeModifiers[0]);
             }
             break;
         }
         case 2:{ //Flower
             AllBullets = BulletShapes.Flower(BB.TypeModifiers[1],BB.TypeModifiers[2],BB.TypeModifiers[3],BB.SpawnPoint.X,BB.SpawnPoint.Y,BB.Speed,BB.Size);
             if(BB.TypeModifiers[0] != 0) {
                 AllBullets = BulletShapes.Rotate(AllBullets, BB.TypeModifiers[0]);
             }
             break;
         }

         case 3:{ //Random Spread
             AllBullets = BulletShapes.RandomSpread(BB.TypeModifiers[1],BB.TypeModifiers[2],BB.TypeModifiers[3],BB.SpawnPoint.X,BB.SpawnPoint.Y,BB.Speed,BB.Size);
             if(BB.TypeModifiers[0] != 0) {
                 AllBullets = BulletShapes.Rotate(AllBullets, BB.TypeModifiers[0]);
             }
             break;
         }
     }

     BB.Used = true;
     return AllBullets;
    }

    /**
     * Updates position of bullet based on speed
     * @param TimePassed
     */
    public void Update(double TimePassed){
        VL.Increment(TimePassed);
        CheckGrid();
    }

    /**
     * Draws bullet
     * @param canvas
     * @param ScaleX
     * @param ScaleY
     * @param C
     */
    public void Draw(Canvas canvas, double ScaleX, double ScaleY, Context C){
        Point DrawPoint = new Point(VL.P1.X * ScaleX,VL.P1.Y * ScaleY);
        RectF Bullet = new RectF((float)(DrawPoint.X - (ScaleX*(Rad/10))),(float)(DrawPoint.Y - ScaleY*(Rad/10)), (float)(DrawPoint.X + ScaleX*(Rad/10)), (float)(DrawPoint.Y + ScaleY*(Rad/10)));
        Paint P = new Paint();
        P.setColor(Color.RED);
        canvas.drawOval(Bullet,P);
    }

    /**
     * Sets the velocity of an existing bullet
     * @param XV XVelocity
     * @param YV YVelocity
     */
    public void SetVelocity(double XV, double YV){
        VL = new VectorLine(VL.P1,XV,YV);
    }

    /**
     * Checks grid location of the bullet,
     * Used to simplify bullet collision checking
     * Sets bullet to dead if outside of range
     */
    public void CheckGrid(){
        Point CurrentLoc = VL.P1;
        XGrid = (int)(CurrentLoc.X + 0.5);
        YGrid = (int)(CurrentLoc.Y + 0.5);
        if(XGrid < -5 || XGrid > 55 || YGrid < -5 || YGrid > 55){
            Dead = true;
        }
    }
}

package javaprojects.finalcoursework.beatinvaders.Game.Buffer;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;

/**
 * Buffered version of enemy, does not keep track of cooldown, location or vectors
 */
public class BufferedEnemy {
    public int SpawnTime;
    public int Type;
    public  Point SpawnPoint;
    public  Point GoalPoint;
    public  double Size;
    public  int Health;
    public  double Speed;

    public  int BulletType;
    public  int BulletSpeed;
    public  int BulletSize;
    public  int BulletCooldown;
    public int[] BulletModifiers;

    public  boolean AimedBullet = false;

    public  boolean Used = false;

    /**
     * Takes in construct string from file,
     * Splits construct into enemy and bullet details
     * @param EnemyConstruct
     */
    public BufferedEnemy(String EnemyConstruct) {
        String[] SplitConstruct = EnemyConstruct.split(":");
        BuildEnemy(SplitConstruct[0]);
        BuildBullet(SplitConstruct[1]);
    }

    /**
     * Sets parameters based on construct for enemy
     * @param Construct
     */
    private void BuildEnemy(String Construct) {
        String[] ShipDetails = Construct.split(",");
        SpawnTime = Integer.parseInt(ShipDetails[0]);
        int StartX = Integer.parseInt(ShipDetails[1]);
        int StartY = Integer.parseInt(ShipDetails[2]);
        SpawnPoint = new Point(StartX, StartY);
        Type = Integer.parseInt(ShipDetails[3]);
        StartX = Integer.parseInt(ShipDetails[4]);
        StartY = Integer.parseInt(ShipDetails[5]);
        GoalPoint = new Point(StartX, StartY);
        Speed = Integer.parseInt(ShipDetails[6]);
        Size = Integer.parseInt(ShipDetails[7]);
        Health = Integer.parseInt(ShipDetails[8]);
        BulletCooldown = Integer.parseInt(ShipDetails[9]);
    }

    /**
     * Sets parameters based on construct for bullet
     * @param Construct
     */
    private void BuildBullet(String Construct) {
        String[] BulletDetails = Construct.split(",");
        BulletType = Integer.parseInt(BulletDetails[0]);
        int Destination = Integer.parseInt(BulletDetails[1]);
        if (Destination == 1) {
            AimedBullet = true;
        }
        BulletSpeed = Integer.parseInt(BulletDetails[2]);
        BulletSize = Integer.parseInt(BulletDetails[3]);
        int ExtraModifiers = BulletDetails.length;
        if(ExtraModifiers >= 5){
            BulletModifiers = new int[ExtraModifiers - 4];
            for(int i = 0; i < BulletModifiers.length;i++){
                BulletModifiers[i] = Integer.parseInt(BulletDetails[4+i]);
            }
        }
    }
}

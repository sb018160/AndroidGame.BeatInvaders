package javaprojects.finalcoursework.beatinvaders.Game.Objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.R;

import java.util.ArrayList;

/**
 * Ship that player controls, holds functions to move to different points
 */
public class PlayerShip extends Obstacle {

    private int XGrid;
    private int YGrid;

    private Point Goal;
    private Point Anchor;

    /**
     * Creates ship at set location
     * @param XLoc coordinates
     * @param YLoc coordinates
     */
    public PlayerShip(int XLoc, int YLoc) {
        Location = new Point(XLoc,YLoc);
        this.Speed = 1000;
        this.Size = 1;
        Goal = new Point(Location);
    }

    /**
     * Draws ship to the screen
     * @param canvas
     * @param ScaleX
     * @param ScaleY
     * @param C
     */
    public void Draw(Canvas canvas, double ScaleX, double ScaleY, Context C){
            Point DrawPoint = new Point(Location.X * ScaleX,Location.Y * ScaleY);
            RectF Bullet = new RectF((float)(DrawPoint.X - ScaleX),(float)(DrawPoint.Y - ScaleY), (float)(DrawPoint.X + ScaleX), (float)(DrawPoint.Y + ScaleY));
            Bitmap Player = BitmapFactory.decodeResource(C.getResources(), R.drawable.game_player);
            canvas.drawBitmap(Player,null,Bullet,null);
    }

    /**
     * Updates ship by time passed
     * @param Time passed
     */
    public void Update(double Time){
        if(!Location.equals(Goal))
        {
            MoveToPoint(Time);
            CheckGrid();
        }
    }

    /**
     * Checks position on grid to be used for collision detection
     */
    private void CheckGrid() {
        XGrid = (int)(Location.X + 0.5);
        YGrid = (int)(Location.Y + 0.5);
    }

    /**
     * Moves ship to set point
     * @param Time passed
     */
    private void MoveToPoint(double Time) {
        double DistX = Goal.X;
        double DistY = Goal.Y;
        double AbsX,AbsY;

        double MaxSpeed = Time*Speed;

        DistX -= Location.X;
        DistY -= Location.Y;
        double Ratio;

        AbsX = Math.abs(DistX);
        AbsY = Math.abs(DistY);
        if(AbsX>MaxSpeed && AbsY > MaxSpeed)
        {
            Ratio = AbsX/(AbsX+AbsY);
            Ratio *= Ratio;

            Location.X += (Ratio * MaxSpeed * (DistX/AbsX));
            Location.Y += ((1-Ratio) * MaxSpeed * (DistY/AbsY));
            return;
        }
        else
        {
            if(AbsY>MaxSpeed)
            {
                Location.Y += (MaxSpeed * (DistY/AbsY));
                Location.X = Goal.X;
                return;
            }
            if(AbsX>MaxSpeed)
            {
                Location.X += (MaxSpeed * (DistX/AbsX));
                Location.Y = Goal.Y;
                return;
            }
            Location.X = Goal.X;
            Location.Y = Goal.Y;
            return;
        }
    }

    /**
     * Checks ship hitbox against given bullets
     * @param AllBullets to check against
     * @return true if hit
     */
    public boolean CheckHitbox(ArrayList<Bullet> AllBullets){
        for(Bullet B: AllBullets) {
            Point BLoc = B.VL.P1;
            if (B.XGrid == XGrid && B.YGrid == YGrid) {
                if (!B.Dead) {
                    double dx = Location.X - BLoc.X;
                    double dy = Location.Y - BLoc.Y;
                    double radiusSum = Size/10.0 + B.Rad/10;
                    if(dx * dx + dy * dy <= radiusSum * radiusSum){
                        B.Dead = true;
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Translates goal point for ship to move
     * @param point translation point
     */
    public void TranslatePoint(Point point) {
        Goal = new Point(Anchor);
        Point NewGoal = new Point(Goal.X+point.X, Goal.Y+point.Y);
        if(NewGoal.X > 0.2 && NewGoal.X < 49.8 && NewGoal.Y > 0.2 & NewGoal.Y < 49.8){
            Goal = new Point(NewGoal);
        }else{
            Goal = new Point(Location);
        }
    }

    /**
     * Sets anchor point for ship to translate from
     */
    public void SetAnchor() {
        Anchor = new Point(Location);
    }
}

package javaprojects.finalcoursework.beatinvaders.Game.Objects;

import android.content.Context;
import android.graphics.Canvas;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;

/**
 * Abstract obstacle class which holds location, speed and size
 */
public abstract class Obstacle {
    public Point Location;
    int Speed;
    int Size;

    public abstract void Update(double time);

    public abstract void Draw(Canvas canvas, double ScaleX, double ScaleY, Context C);


}

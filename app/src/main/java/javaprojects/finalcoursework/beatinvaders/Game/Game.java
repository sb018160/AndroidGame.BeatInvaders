package javaprojects.finalcoursework.beatinvaders.Game;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Game.Buffer.BufferManager;
import javaprojects.finalcoursework.beatinvaders.Game.Objects.Bullet;
import javaprojects.finalcoursework.beatinvaders.Game.Objects.Enemy;
import javaprojects.finalcoursework.beatinvaders.Game.Objects.PlayerShip;
import javaprojects.finalcoursework.beatinvaders.Game.Objects.Spawner;
import javaprojects.finalcoursework.beatinvaders.Song.Song;
import javaprojects.finalcoursework.beatinvaders.Song.SongPlayer;

/**
 * Overall Game object which holds all entities and handles updating and drawing these entities.
 */
public class Game {
    volatile boolean DrawFlag = true;
    volatile boolean UpdateFlag = false;

    Handler GameHandler;
    Context C;
    double CanvasX, CanvasY;
    double ScaleX, ScaleY;

    Point AnchorPoint;

    SongPlayer Player;

    Song PlayingMap;

    BufferManager Buffers;

    ArrayList<Enemy> Enemies;
    ArrayList<Bullet> LoadedBullets;
    ArrayList<Spawner> Spawners;

    Thread BufferThread;
    Thread UpdateThread;


    volatile PlayerShip PShip;

    double BPMConversion;
    double GameTimer = 0;
    double LevelLength = 0;

    long LastUpdateTime;
    long CurrentUpdateTime;
    double SecondsPassed;

    int HitCount = 0;
    boolean GameOver = false;

    /**
     * Sets up level based on song name and level name, takes in game handler to communicate with activity level when game ends
     * @param SongName for level
     * @param LevelName of level
     * @param gameHandler to communicate with activity
     * @param C
     */
    public Game(String SongName, String LevelName, Handler gameHandler, Context C) {
        this.C = C;
        GameHandler = gameHandler;
        try {
            SetSong(SongName, LevelName);
            Player = new SongPlayer();
            Player.SetSong(PlayingMap);
            BPMConversion = PlayingMap.SongBPM / 60.0;
            LevelLength = (PlayingMap.LengthTotalSeconds * BPMConversion);
            InitiateObjects();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Sets up all required objects and parameters at the start of the level
     */
    private void InitiateObjects() {
        Enemies = new ArrayList<>();
        LoadedBullets = new ArrayList<>();
        Spawners = new ArrayList<>();

        Buffers = new BufferManager(Enemies, LoadedBullets, Spawners, PlayingMap, C);
        PShip = new PlayerShip(25, 40);

        BufferThread = new Thread("Buffer Thread") {
            @Override
            public void run() {
                UpdateBuffer();
            }
        };

        UpdateThread = new Thread("Update Thread") {
            @Override
            public void run() {
                UpdateObjects(SecondsPassed);
            }
        };
    }

    /**
     * Sets up Song objects by getting file for the song and level details
     * @param SongName for level
     * @param LevelName of level
     * @throws IOException if files not found
     */
    private void SetSong(String SongName, String LevelName) throws IOException {
        File SongDir = Environment.getExternalStorageDirectory();
        SongDir = new File(SongDir, "Beat Invaders");
        SongDir = new File(SongDir, "Songs");
        SongDir = new File(SongDir, SongName);
        SongDir = new File(SongDir, LevelName);
        FileReader fileReader = new FileReader(new File(SongDir, "SongInfo.txt"));
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        PlayingMap = new Song(bufferedReader.readLine());
    }

    /**
     * Sets size of canvas, to be used for scaling
     * @param width of canvas
     * @param height of canvas
     */
    public void SetSize(double width, double height) {
        CanvasX = width;
        CanvasY = height;

        ScaleX = CanvasX / 50;
        ScaleY = CanvasY / 50;
    }

    /**
     * Draws all objects onto the canvas
     * @param canvas to draw on
     */
    public void Draw(Canvas canvas) {
        if (!UpdateFlag) {
            PShip.Draw(canvas, ScaleX, ScaleY, C);
            for (Enemy E : Enemies) {
                E.Draw(canvas, ScaleX, ScaleY, C);
            }
            for (Bullet B : LoadedBullets) {
                B.Draw(canvas, ScaleX, ScaleY, C);
            }
            DrawFlag = false;
            UpdateFlag = true;
        }
    }

    /**
     * Moves all objects in game by the set difference
     * @param Difference
     */
    private void MoveGame(double Difference) {
        PShip.Update(Difference); //Update Player
        ArrayList<Integer> DeadList = new ArrayList<>();
        int i = 0;
        int DeletedNo = 0;
        for (Spawner S : Spawners) { //Update Enemies
            S.Update(Difference);
            if (S.Dead) {
                DeadList.add(i);
            }
            if (S.ReadyToSpawn) {
                LoadedBullets.addAll(S.SpawnShots());
            }
            i++;
        }
        for (Integer I : DeadList) {
            Spawners.remove(I - DeletedNo);
            DeletedNo++;
        }


        DeadList.clear();
        i = 0;
        for (Enemy E : Enemies) { //Update Enemies
            E.Update(Difference);
            E.CheckGrid();
            if (!E.Alive) {
                DeadList.add(i);
            }
            if (E.ReadyToFire) {
                LoadedBullets.addAll(E.FireBullet(PShip.Location));
            }
            i++;
        }

        DeletedNo = 0;
        for (Integer I : DeadList) {
            Enemies.remove(I - DeletedNo);
            DeletedNo++;
        }

        DeadList.clear();
        i = 0;
        for (Bullet B : LoadedBullets) { //Update Bullets
            B.Update(Difference);
            if (B.Dead) {
                DeadList.add(i);
            }
            i++;
        }

        DeletedNo = 0;
        for (Integer I : DeadList) {
            LoadedBullets.remove(I - DeletedNo);
            DeletedNo++;
        }
    }

    /**
     * Updates player ship coordinates with touch coordinates and touch state
     * @param State of touch
     * @param point of touch
     */
    public void UpdateMouse(int State, Point point) {
        switch (State) {
            case 0://Pressed
            {
                AnchorPoint = new Point(point);
                PShip.SetAnchor();
                break;
            }
            case 1://Dragged
            {
                Point DragPoint = new Point(point);
                Point DMove = new Point(DragPoint.X - AnchorPoint.X, DragPoint.Y - AnchorPoint.Y);
                DMove = new Point(DMove.X / ScaleX, DMove.Y / ScaleY);
                PShip.TranslatePoint(DMove);
                break;
            }
            case 2: {//Released

                break;
            }
        }
    }

    /**
     * Overall update function, splits updating into two threads, one for updating buffers,
     * another for updating all existing objects. Waits for both to finish before proceeding.
     */
    public void Update() {
        while (DrawFlag) {
            android.os.SystemClock.sleep(1);
        }

        if(!GameOver) {
            CurrentUpdateTime = Player.GetTime();
            SecondsPassed = (CurrentUpdateTime - LastUpdateTime) / 1000.0;
            SecondsPassed = BPMConversion * SecondsPassed;
            GameTimer += SecondsPassed;
            if (GameTimer < LevelLength) {
                ExecutorService service = Executors.newFixedThreadPool(2);
                service.execute(BufferThread);
                service.execute(UpdateThread);
                service.shutdown();
                try {
                    service.awaitTermination(1, TimeUnit.MINUTES);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
                LoadedBullets.addAll(Buffers.ReadyToSpawnBullets);
                Spawners.addAll(Buffers.ReadyToSpawnSpawners);
                LastUpdateTime = CurrentUpdateTime;
                UpdateFlag = false;
                DrawFlag = true;
            } else {
                UpdateFlag = false;
                DrawFlag = true;
                FinishGame();
            }
        }
    }

    /**
     * Starts music player and gets position of the player
     */
    public void Start() {
        LastUpdateTime = Player.GetTime();
        Player.PlaySong();
    }

    /**
     * Pauses music player
     */
    public void Pause() {
        Player.PauseSong();
    }

    /**
     * Updates BufferManager with new gametime
     */
    public void UpdateBuffer() {
        Buffers.CheckBuffer(GameTimer);
    }

    /**
     * Updates all objects by time passed
     * @param secondsPassed to update with
     */
    public void UpdateObjects(double secondsPassed) {
        MoveGame(secondsPassed);
        if(PShip.CheckHitbox(LoadedBullets)){
            HitCount++;
        }
    }

    /**
     * Sends message to handler when game is finished with information about how many times player
     * is hit.
     */
    public void FinishGame(){
        GameOver = true;
        Message M = new Message();
        M.what = HitCount;
        GameHandler.sendMessage(M);
    }

    /**
     * Empties all arrays and deletes all objects
     */
    public void DeleteObjects() {
        Buffers = null;
        LoadedBullets.clear();
        Spawners.clear();
        Enemies.clear();
        Player.Delete();
    }
}

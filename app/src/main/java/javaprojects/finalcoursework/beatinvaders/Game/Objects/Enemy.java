package javaprojects.finalcoursework.beatinvaders.Game.Objects;


import android.content.Context;
import android.graphics.Canvas;

import javaprojects.finalcoursework.beatinvaders.Extras.AngleMathematics;
import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Extras.VectorLine;
import javaprojects.finalcoursework.beatinvaders.Game.Buffer.BufferedEnemy;

import java.util.ArrayList;

/**
 * Class for enemy object that game uses, enemies act like moving spawners with a setting that aims
 * the angle of shots to try and hit the player.
 */
public class Enemy {

    int Type;
    VectorLine Path;
    double Size;
    int Health;

    int BulletType;
    int BulletSpeed;
    int BulletSize;
    double BulletCooldown;
    double BulletTimer = BulletCooldown;
    int[] BulletModifiers;

    boolean AimedBullet;

    public boolean Alive;
    public boolean ReadyToFire;

    int XGrid;
    int YGrid;

    /**
     * Constructs enemy from buffered enemy
     * @param BE Buffered enemy to use
     */
    public Enemy(BufferedEnemy BE) {
        Type = BE.Type;
        Point Location = BE.SpawnPoint;
        Point GoalPoint = BE.GoalPoint;
        Size = BE.Size;
        Health = BE.Health;
        double Speed = BE.Speed;
        BulletType = BE.BulletType;
        BulletSpeed = BE.BulletSpeed;
        BulletSize = BE.BulletSize;
        BulletCooldown = BE.BulletCooldown;
        AimedBullet = BE.AimedBullet;
        BulletModifiers = BE.BulletModifiers;

        Alive = true;
        ReadyToFire = false;

        BE.Used = true;
        Path = AngleMathematics.CalculateVector(Location, GoalPoint, Speed);
    }

    /**
     * Draws self to canvas
     * @param canvas
     * @param ScaleX
     * @param ScaleY
     * @param C
     */
    public void Draw(Canvas canvas, double ScaleX, double ScaleY, Context C){

    }

    /**
     * Updates self by time passed
     * @param difference in time
     */
    public void Update(double difference) {
        Path.Increment(difference);
        BulletTimer -= difference;
        if(BulletTimer <= 0){
            ReadyToFire = true;
        }
    }

    /**
     * Fires set bullet
     * @param ShipLoc location of player
     * @return list of bullets
     */
    public ArrayList<Bullet> FireBullet(Point ShipLoc){
        ArrayList<Bullet> Fired = new ArrayList<>();
        VectorLine  BulletVector;
        if(AimedBullet){
             BulletVector = AngleMathematics.CalculateVector(Path.P1,ShipLoc,BulletSpeed);
        } else {
             BulletVector = new VectorLine(Path.P1,0,BulletSpeed);
             BulletVector = AngleMathematics.RotateVector(BulletVector,BulletModifiers[0]);
        }
        Fired.add(new Bullet(BulletVector, BulletSize));
        BulletTimer = BulletCooldown;
        ReadyToFire = false;
        return Fired;
    }

    /**
     * Sets enemy as dead if outside of screen
     */
    public void CheckGrid(){
        Point CurrentLoc = Path.P1;
        XGrid = (int)CurrentLoc.X;
        YGrid = (int)CurrentLoc.Y;
        if(XGrid < 0 || XGrid > 50 || YGrid < 0 || YGrid > 50){
            Alive = false;
        }
    }

    /**
     * Checks hitbox of enemy against bullets given
     * @param AllBullets bullets to check against
     * @return true if hit
     */
    public boolean CheckHitbox(ArrayList<Bullet> AllBullets){
        Point Location = Path.P1;
        for(Bullet B: AllBullets) {
            Point BLoc = B.VL.P1;
            if (B.XGrid == XGrid && B.YGrid == YGrid) {
                if (!B.Dead) {
                    double dx = Location.X - BLoc.X;
                    double dy = Location.Y - BLoc.Y;
                    double radiusSum = Size/2 + B.Rad;
                    if(dx * dx + dy * dy <= radiusSum * radiusSum){
                        System.out.println("E Hit\n");
                        B.Dead = true;
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

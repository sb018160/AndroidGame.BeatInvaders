package javaprojects.finalcoursework.beatinvaders.Game.Objects;



import java.util.ArrayList;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Game.Buffer.BufferedBullet;

/**
 * Spawner class to be used in game, spawns a set of bullets at location and modifiers
 */
public class Spawner {

    public int SpawnTime;

    Point Location;
    double TimeToLive;
    double SpawnCooldown;
    double CooldownCounter;
    int SpawnerType;
    int[] SpawnerModifiers;

    BufferedBullet CurrentSpawnee;
    int SpawnCount = 0;
    double SpawneeUpdate;

    public boolean ReadyToSpawn;

    public boolean Dead;

    /**
     * Constructs spawner from string gotten from spawner file
     * @param Construct string form file
     */
    public Spawner(String Construct){
        String[] SplitConstruct = Construct.split(":");
        SetSpawner(SplitConstruct[0]);
        SetSpawnee(SplitConstruct[1]);
    }

    /**
     * Copies spawner from existing spawner
     * @param S
     */
    public Spawner(Spawner S){
        Location = new Point(S.Location);
        TimeToLive = S.TimeToLive;
        SpawnCooldown = S.SpawnCooldown;
        CooldownCounter = S.CooldownCounter;
        SpawnerType = S.SpawnerType;
        SpawnerModifiers = S.SpawnerModifiers;
        CurrentSpawnee = S.CurrentSpawnee;
        SpawneeUpdate = 0;

        Dead = false;
        ReadyToSpawn = false;
    }

    /**
     * Sets the spawnee of the spawner to the bullet constructed from the string
     * @param SpawneeString from file
     */
    private void SetSpawnee(String SpawneeString) {
        String[] Parameters = SpawneeString.split(",");
        int SpawneeType = Integer.parseInt(Parameters[0]);
        int SpawneeSpeed = Integer.parseInt(Parameters[1]);
        int SpawneeSize = Integer.parseInt(Parameters[2]);
        int[] SpawneeModifiers = new int[0];
        int ExtraModifiers = Parameters.length;
        if(ExtraModifiers >= 4){
            SpawneeModifiers = new int[ExtraModifiers - 3];
            for(int i = 0; i < SpawneeModifiers.length;i++){
                SpawneeModifiers[i] = Integer.parseInt(Parameters[3+i]);
            }
        }
        CurrentSpawnee = new BufferedBullet(0,SpawneeType,Location,SpawneeSpeed,SpawneeSize,SpawneeModifiers);
    }

    /**
     * Sets up parameters for spawner from string
     * @param SpawnerString for constructing
     */
    private void SetSpawner(String SpawnerString) {
        String[] Parameters = SpawnerString.split(",");
        this.SpawnTime = Integer.parseInt(Parameters[0]);
        double XLoc = Integer.parseInt(Parameters[1]);
        double YLoc = Integer.parseInt(Parameters[2]);
        this.SpawnerType = Integer.parseInt(Parameters[3]);
        Location = new Point(XLoc,YLoc);
        this.TimeToLive = Integer.parseInt(Parameters[4]);
        int CooldownFull = Integer.parseInt(Parameters[5]);
        int CooldownFraction = Integer.parseInt(Parameters[6]);
        double Fraction = 0;
        if(CooldownFraction != 0){
            Fraction = 1.0/CooldownFraction;
        }
        SpawnCooldown = CooldownFull + Fraction;

        SpawnerModifiers = new int[2];
        SpawnerModifiers[0] = Integer.parseInt(Parameters[7]);
        SpawnerModifiers[1] = Integer.parseInt(Parameters[8]);


        CooldownCounter = SpawnCooldown;
        ReadyToSpawn = false;
        Dead = false;
    }

    /**
     * Based on spawner type, modifies the spawenee per spawning
     */
    private void ModifySpawnee(){
        SpawnCount++;
        switch(SpawnerType){
            case 0:{//Does nothing extra
                break;
            }
            case 1:{ //Rotates
                CurrentSpawnee.TypeModifiers[0] += SpawnerModifiers[0];
                break;
            }
            case 2:{ //Offsets
                CurrentSpawnee.SpawnPoint = new Point(CurrentSpawnee.SpawnPoint.X + (SpawnerModifiers[0]),CurrentSpawnee.SpawnPoint.Y + (SpawnerModifiers[1]));
                break;
            }
        }
    }

    /**
     * Updates parameters by time passed
     * @param TimePassed between updates
     */
    public void Update(double TimePassed){
        TimeToLive -= TimePassed;
        CooldownCounter -= TimePassed;
        CheckCooldown();
        if(TimeToLive <= SpawnCooldown){
            Dead = true;
        }
    }

    /**
     * Spawns set spawnee shots
     * @return list of bullets
     */
    public ArrayList<Bullet> SpawnShots(){
        ReadyToSpawn = false;
        ModifySpawnee();
        double Timestamp = System.nanoTime();
        ArrayList<Bullet> SpawnedShots = Bullet.CreateBullets(CurrentSpawnee);
        // Updates spawned bullets by the amount of time taken to create them
        for(Bullet B: SpawnedShots){
            double NewStamp = SpawneeUpdate+(System.nanoTime() - Timestamp)/1E9;
            B.Update(NewStamp);
        }
        CooldownCounter += SpawnCooldown + (System.nanoTime() - Timestamp)/1E9;
        CheckCooldown();
        // If already ready to spawn, then spawns bullets again
        if(ReadyToSpawn){
            SpawnedShots.addAll(SpawnShots());
        }
        return SpawnedShots;
    }

    /**
     * Checks whether the spawner is ready to spawn.
     */
    private void CheckCooldown() {
        if(CooldownCounter <= 0){
            SpawneeUpdate = -CooldownCounter;
            ReadyToSpawn = true;
        }
    }
}

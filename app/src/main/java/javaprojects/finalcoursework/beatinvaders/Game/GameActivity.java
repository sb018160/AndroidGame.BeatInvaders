package javaprojects.finalcoursework.beatinvaders.Game;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javaprojects.finalcoursework.beatinvaders.R;

/**
 * Activity for the game
 */
public class GameActivity extends AppCompatActivity {

    static Handler GameHandler;

    String SongName;
    String LevelName;

    boolean Editor = false;

    int HighScore;

    Thread GThread;
    Runnable UIUpdate;

    volatile boolean Running;

    GameView GView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        SetUpGame();
    }

    /**
     * Sets up game objects and passes all required references
     */
    private void SetUpGame() {

        setContentView(R.layout.activity_game);
        SetUpHandler();
        Intent I = getIntent();
        SongName = I.getStringExtra("SongName");
        LevelName = I.getStringExtra("LevelName");
        Editor = I.getBooleanExtra("Flag", false);
        GView = findViewById(R.id.GameCanvasView);
        Context C = getBaseContext();
        SetUpThread();
        Game TheGame = new Game(SongName, LevelName, GameHandler, C);
        GView.SetUp(TheGame);
        SetButtons();
        ShowPauseMenu();
    }

    /**
     * Sets up thread to be used for updating the game
     */
    private void SetUpThread() {
        UIUpdate = new Runnable() {
            @Override
            public void run() {
                GView.WaitingForDraw = true;
                GView.invalidate();
            }
        };

        GThread = new Thread("GameThread") {
            @Override
            public void run() {
                while (Running) {
                    GView.UpdateGame();
                    if (!GView.WaitingForDraw) { //If already waiting don't tell it to draw
                        runOnUiThread(UIUpdate); //Required to work with 7.0 and below
                    }
                }
            }
        };
        GThread.start();
    }

    /**
     * Sets up pause button
     */
    private void SetButtons() {
        Button PauseButton = findViewById(R.id.GamePauseButton);
        PauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowPauseMenu();
            }
        });
    }

    /**
     * Shows and sets up pause menu
     */
    private void ShowPauseMenu() {
        PauseThread();
        final ConstraintLayout PauseLayout = findViewById(R.id.GamePauseMenuLayout);
        PauseLayout.setVisibility(View.VISIBLE);

        Button StartButton = findViewById(R.id.GamePauseMenuStart);
        Button RestartButton = findViewById(R.id.GamePauseMenuRestart);
        Button QuitButton = findViewById(R.id.GamePauseMenuQuit);
        Button MenuButton = findViewById(R.id.GamePauseMenuMenu);
        TextView PauseText = findViewById(R.id.GamePauseMenuText);

        TextView ScoreText = findViewById(R.id.GamePauseMenuScoreView);
        ScoreText.setText("Top Score: "+ GetScore());
        PauseText.setText(LevelName);

        if (Editor) {
            QuitButton.setText("Edit");
        }

        StartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PauseLayout.setVisibility(View.INVISIBLE);
                StartThread();
            }
        });

        RestartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PauseLayout.setVisibility(View.INVISIBLE);
                SetUpGame();
            }
        });

        QuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuitActivity();
            }
        });

        MenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PauseThread();
                BackToMenu();
            }
        });
    }

    /**
     * finishes activity and calls for main activity to either go to menu, quit app or go to editor
     */
    private void QuitActivity() {
        PauseThread();
        if (Editor) {
            Intent I = new Intent();
            I.putExtra("SongName", SongName);
            I.putExtra("LevelName", LevelName);
            I.putExtra("What", 43);
            setResult(3, I);
            finish();
        } else {
            Intent I = new Intent();
            setResult(-1, I);
            finish();
        }
    }

    /**
     * Creates handler for communication
     */
    private void SetUpHandler() {
        GameHandler = new Handler() {
            public void handleMessage(Message m) {
                GameEnded(m.what);
            }
        };
    }

    /**
     * Pauses game thread
     */
    private void PauseThread() {
        Running = false;
        GView.TheGame.Pause();
    }

    /**
     * Starts/resumes game thread
     */
    private void StartThread() {
        GView.TheGame.Start();
        Running = true;
        SetUpThread();
    }

    /**
     * Called when message received about game ending, saves score for level
     *
     * @param Score to be saved
     */
    private void GameEnded(int Score) {
        PauseThread();
        android.os.SystemClock.sleep(10);
        GView.TheGame.DeleteObjects();
        if(Score < HighScore) {
            SharedPreferences sharedPref = getSharedPreferences("LevelScores", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("" + SongName + "__" + LevelName, Score);
            editor.commit();
        }
        ShowEndGameMenu();
    }

    private void BackToMenu() {
        Intent I = new Intent();
        I.putExtra("What", 41);
        setResult(1, I);
        finish();
    }

    /**
     * Same as pause menu but removes "start" button as the game has ended.
     */
    private void ShowEndGameMenu() {
        final ConstraintLayout PauseLayout = findViewById(R.id.GamePauseMenuLayout);
        PauseLayout.setVisibility(View.VISIBLE);

        Button StartButton = findViewById(R.id.GamePauseMenuStart);
        Button RestartButton = findViewById(R.id.GamePauseMenuRestart);
        Button QuitButton = findViewById(R.id.GamePauseMenuQuit);
        Button MenuButton = findViewById(R.id.GamePauseMenuMenu);
        TextView PauseText = findViewById(R.id.GamePauseMenuText);

        PauseText.setText(LevelName);
        StartButton.setVisibility(View.INVISIBLE);
        
        TextView ScoreText = findViewById(R.id.GamePauseMenuScoreView);
        ScoreText.setText("Top Score: "+ GetScore());

        RestartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PauseLayout.setVisibility(View.INVISIBLE);
                SetUpGame();
            }
        });

        QuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editor = false;
                QuitActivity();
            }
        });

        MenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PauseThread();
                BackToMenu();
            }
        });
    }

    /**
     * Gets score from shared preferences
     * @return 0 if no score set
     */
    private int GetScore() {
        SharedPreferences sharedPref = getSharedPreferences("LevelScores", Context.MODE_PRIVATE);
        HighScore = sharedPref.getInt(""+SongName+"__"+LevelName,999);
        return HighScore;
    }

    /**
     * Goes back to either editor or menu when pressed
     */
    @Override
    public void onBackPressed() {
        if (Editor) {
            QuitActivity();
        } else {
            PauseThread();
            Intent I = new Intent();
            I.putExtra("What", 41);
            setResult(1, I);
            finish();
        }
    }


}

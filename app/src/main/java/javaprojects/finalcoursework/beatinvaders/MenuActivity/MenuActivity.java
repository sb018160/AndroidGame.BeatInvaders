package javaprojects.finalcoursework.beatinvaders.MenuActivity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import javaprojects.finalcoursework.beatinvaders.R;

public class MenuActivity extends AppCompatActivity {

    MessageHandler MHandler;
    Context C;

    MenuView Menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        C = getBaseContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        Menu = findViewById(R.id.MenuView);
        SetUpHandler();
        Menu.SetUp(MHandler);
    }

    /**
     * Sets up handler to be passed to MenuView
     */
    private void SetUpHandler() {
        MHandler = new MessageHandler(this, C);
    }

    /**
     * Sets level to be played and finishes activity
     * @param SongName to play
     * @param LevelName to play
     */
    public void SetLevel(String SongName, String LevelName) {
        Intent I = new Intent();
        I.putExtra("What",14);
        I.putExtra("SongName", SongName);
        I.putExtra("LevelName",LevelName);
        setResult(4,I);
        finish();
    }

    /**
     * Quits app
     */
    public void Quit(){
        Intent I = new Intent();
        setResult(-1,I);
        finish();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Sets song/level to be edited
     * @param SongName
     * @param LevelName
     * @param Flag
     */
    public void SetEditor(String SongName, String LevelName, boolean Flag) {
    Intent I = new Intent();
    if(Flag){
        I.putExtra("What",12);
        setResult(2,I);
    }else{
        I.putExtra("What",13);
        I.putExtra("SongName",SongName);
        I.putExtra("LevelName",LevelName);
        setResult(3,I);
    }
    finish();
    }

    public void onBackPressed() {
        Quit();
    }
}


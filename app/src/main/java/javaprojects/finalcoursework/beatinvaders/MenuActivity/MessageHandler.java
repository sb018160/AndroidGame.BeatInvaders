package javaprojects.finalcoursework.beatinvaders.MenuActivity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import javaprojects.finalcoursework.beatinvaders.R;

final class MessageHandler extends Handler {
    MenuActivity MA;
    Context C;

    public MessageHandler(MenuActivity MA, Context C) {
        this.MA = MA;
        this.C = C;
    }

    /**
     * Either quits, opens level or editor based on what buttons were pressed
     * @param msg
     */
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case 14: {
                Bundle MsgData = msg.getData();
                String SongName = MsgData.getString("SongName");
                String LevelName = MsgData.getString("LevelName");
                MA.SetLevel(SongName, LevelName);
                break;
            }
            case 13:{
                Bundle MsgData = msg.getData();
                String SongName = MsgData.getString("SongName");
                String LevelName = MsgData.getString("LevelName");
                MA.SetEditor(SongName, LevelName, false);
                break;
            }
            case 12:{
                MA.SetEditor("","",true);
                break;
            }
            case -1:{
                MA.Quit();
                break;
            }
            default:{
break;
            }
        }
    }
}
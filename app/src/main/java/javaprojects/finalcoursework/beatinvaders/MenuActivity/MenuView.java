package javaprojects.finalcoursework.beatinvaders.MenuActivity;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.File;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Menu.MainMenu;

public class MenuView extends View {

    Context C;

    double Width;
    double Height;

    String Construct = "Levels,Editor,Quit:New Level,Existing Level,Back:";

    MainMenu Menu;

    Point DownPoint;
    Point DragPoint;

    Handler MHander;

    public MenuView(Context context, AttributeSet atts) {
        super(context, atts);
        C = context;
    }

    /**
     * Sets up handler and menu object, as well as ontouchlistener
     * @param MessageHandler
     */
    public void SetUp(Handler MessageHandler) {
        MHander = MessageHandler;

        setClickable(true);
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        DownPoint = new Point(event.getX(), event.getY());
                        DragPoint = new Point(DownPoint);
                        return true;
                    }
                    case MotionEvent.ACTION_MOVE: {
                        double Dy;
                        Dy = event.getY() - DragPoint.Y;
                        DragPoint = new Point(event.getX(), event.getY());
                        UpdateButtons(Dy);
                        return true;
                    }
                    case MotionEvent.ACTION_UP: {
                        Point UpPoint = new Point(event.getX(), event.getY());
                        if ((int) UpPoint.X / 10 == (int) DownPoint.X / 10 && (int) UpPoint.Y / 10 == (int) DownPoint.Y / 10) {
                            CheckClick(DownPoint);
                        }
                        break;
                    }
                }
                return false;
            }
        });
        CheckSongs();
    }

    /**
     * Checks for songs in game directory
     */
    private void CheckSongs() {
        File Dir = new File(Environment.getExternalStorageDirectory(),"Beat Invaders");
        Dir = new File(Dir, "Songs");
        String[] Songs = Dir.list();
        String Levels = "";
        if(Songs != null) {
            for (String Song : Songs) {
                    Levels += Song + ",";
            }
        }
        Levels += "Back";
        Construct += Levels;
    }

    /**
     * Updates buttons by translating based on DY
     * @param dy
     */
    private void UpdateButtons(double dy) {
        Menu.Translate(0, dy);
        invalidate();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        // the height will be set at this point
        Height = getMeasuredHeight();
        Width = getMeasuredWidth();

        Menu = new MainMenu(Construct,new Point(0,0),new Point(Width,Height),C);
        Menu.setState(0);
        invalidate();
    }

    /**
     * Draws menu
     * @param canvas
     */
    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint P = new Paint();
        P.setColor(Color.BLACK);
        canvas.drawPaint(P);
        DrawButtons(canvas);
    }

    /**
     * Draws buttons
     * @param canvas
     */
    private void DrawButtons(Canvas canvas) {
        if(Menu!=null) {
            Menu.DrawMenu(canvas);
        }
    }

    /**
     * Checks click location
     * @param point
     */
    private void CheckClick(Point point) {
        if (Menu.CheckClick(point)) {
            CheckMessage();
            invalidate();
        }
    }

    /**
     * Checks message
     */
    private void CheckMessage() {
        Message message = Menu.getMessage();
        if (message.what != 0) {
            MHander.sendMessage(message);
        }
    }
}


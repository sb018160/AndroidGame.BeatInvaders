package javaprojects.finalcoursework.beatinvaders.Song;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Mediaplayer class that uses Song object to get mp3 location as well as implements error checking
 * to avoid interacting with player when not loaded.
 */
public class SongPlayer {

    MediaPlayer MPlayer;
    Song PlayedSong;

    boolean SongSet = false;
    volatile boolean Ready = false;

    public SongPlayer(){
        MPlayer = new MediaPlayer();
    }

    public void SetSong(Song ToPlay) throws IOException {
        PlayedSong = ToPlay;
        MPlayer = new MediaPlayer();
        try {
            MPlayer.setDataSource(PlayedSong.MP3Path);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
        MPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {
                mp.start();
            }
        });
        MPlayer.prepare();
        SongSet = true;
        Ready = true;
    }

    public int GetTime(){
        if(SongSet) {
            return MPlayer.getCurrentPosition();
        }else{
            return -1;
        }
    }

    public void PlaySong(){
        if(MPlayer != null) {
            if (SongSet) {
                while(!Ready){
                    android.os.SystemClock.sleep(1);
                }
                MPlayer.start();
            }
        }

    }

    public void PauseSong(){
        if(MPlayer != null) {
            if (MPlayer.isPlaying()) {
                MPlayer.pause();
            }
        }
    }

    public void SeekTo(int MiliSeconds){
        if(MiliSeconds != MPlayer.getCurrentPosition()) {
            MPlayer.seekTo(MiliSeconds);
        }else{
            PlaySong();
        }

    }

    public void Delete() {
        PlaySong();
        MPlayer.release();
        MPlayer = null;
    }
}

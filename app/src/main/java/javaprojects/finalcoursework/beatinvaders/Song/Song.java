package javaprojects.finalcoursework.beatinvaders.Song;

/**
 * Song object that contains information about mp3 directory, level name and song name as well as bpm and length
 */
public class Song {
    public String TrueName;
    public String LevelName;
    public String Title;
    public String MP3Path;
    public String Artist;

    public int LengthTotalSeconds;
    public int LengthMins;
    public int LengthSec;
    public int SongBPM;

    /**
     * Construct that splits up string inputs
     * @param Input
     */
    public Song(String Input){
        String[] Details = Input.split(",");
        Title = Details[0];
        Artist = Details[1];
        SongBPM = Integer.parseInt(Details[2]);
        LengthTotalSeconds = Integer.parseInt(Details[3]);
        LengthMins = LengthTotalSeconds / 60;
        LengthSec = LengthTotalSeconds % 60;
        MP3Path = Details[4];
        LevelName = Details[5];

        TrueName = LevelName + "-" + Title;
    }

    /**
     * Construct that takes in existing imputs
     * @param title
     * @param artist
     * @param BPM
     * @param length
     * @param Path
     * @param LevelName
     */
    public Song(String title, String artist, int BPM, int length, String Path, String LevelName) {
        Title = title;
        Artist = artist;
        SongBPM = BPM;
        LengthTotalSeconds = length;
        LengthMins = LengthTotalSeconds / 60;
        LengthSec = LengthTotalSeconds % 60;
        MP3Path = Path;
        this.LevelName = LevelName;
        TrueName = LevelName + "-" + Title;
    }

    public String ToString(){
        String Construct = "";
        Construct += Title+","+Artist+","+SongBPM+","+LengthTotalSeconds+","+MP3Path+","+LevelName;
        return Construct;
    }
}

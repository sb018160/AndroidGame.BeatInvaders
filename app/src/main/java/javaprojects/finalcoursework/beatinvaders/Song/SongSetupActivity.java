package javaprojects.finalcoursework.beatinvaders.Song;

import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

import javaprojects.finalcoursework.beatinvaders.R;

/**
 * Activity for creating a new level, checks downloads and music directory for songs and allows the user
 * to pick any mp3 and set details to be used in level.
 */
public class SongSetupActivity extends AppCompatActivity {

    int State = 0;
    Button ContinueButton;
    Button CancelButton;

    Spinner LocationSelector;
    Spinner SongSelector;

    TextView BPMInput;
    TextView LengthInput;
    TextView LevelNameInput;

    String SelectedMP3;
    String Artist;
    String SongName;

    int BPM;
    int Length;

    File Directory;
    File PickedSong;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songsetup);
        Start();
    }

    /**
     * Starts by showing initial buttons and textviews
     */
    private void Start() {
        ContinueButton = findViewById(R.id.SongSetupContinue);
        CancelButton = findViewById(R.id.SongSetupCancel);
        LocationSelector = findViewById(R.id.SongSetupLocation);
        ContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeState(State + 1);
            }
        });
        CancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent();
                I.putExtra("What", 21);
                setResult(1, I);
                finish();
            }
        });
    }

    /**
     * Changes state to i
     *
     * @param i new state
     */
    private void ChangeState(int i) {
        State = i;
        switch (State) {
            case 1: {
                SearchFolder();
                break;
            }
            case 2: {
                PickSong();
                break;
            }
            case 3: {
                CheckInputs();
                break;
            }
        }
    }

    /**
     * Checks whether inputs are correct before proceeding and finalising, displays error if any
     * are empty.
     */
    private void CheckInputs() {
        String BPMtext = BPMInput.getText().toString();
        String Lengthtext = LengthInput.getText().toString();
        String LevelName = LevelNameInput.getText().toString();
        BPM = Integer.parseInt(BPMtext);
        int GivenLength = Integer.parseInt(Lengthtext);
        if (LevelName == "") {
            ShowError("Level name is blank");
            State = 2;
        }
        if (GivenLength > Length + 2) {
            ShowError("Length Too Long");
            State = 2;
        } else {
            BPMInput.setEnabled(false);
            LengthInput.setEnabled(false);
            LevelNameInput.setEnabled(false);
            Length = GivenLength;
            FinaliseSetup();
        }
    }

    /**
     * Displays error message on screen
     *
     * @param ErrorMessage to display
     */
    private void ShowError(String ErrorMessage) {
        TextView ErrorView = findViewById(R.id.SongSetupErrorView);
        ErrorView.setText(ErrorMessage);
        ErrorView.setVisibility(View.VISIBLE);
    }

    /**
     * Puts data in bundle which is put into intent and passed on to main activity when finish() is
     * called
     */
    private void FinaliseSetup() {
        Intent I = new Intent();
        Bundle B = new Bundle();
        String LevelName = LevelNameInput.getText().toString();
        if (SongName == null || SongName == "") {
            SongName = SelectedMP3.substring(0, SelectedMP3.length() - 4);
        }
        B.putString("SongName", SongName);
        B.putString("SongArtist", Artist);
        B.putInt("SongBPM", BPM);
        B.putInt("SongLength", Length);
        B.putString("SongPath", PickedSong.getPath());
        B.putString("LevelName", LevelName);
        I.putExtra("What", 23);
        I.putExtra("Data", B);
        setResult(3, I);
        finish();
    }

    /**
     * Advances stage in selector and causes certain views to become visible and other views to freeze
     * and be un-editable
     */
    private void PickSong() {
        SelectedMP3 = SongSelector.getSelectedItem().toString();
        TextView BPMText = findViewById(R.id.SongBPMView);
        BPMInput = findViewById(R.id.SongSetupBPMInput);
        TextView LengthText = findViewById(R.id.SongLengthView);
        LengthInput = findViewById(R.id.SongSetupLengthInput);
        TextView LevelName = findViewById(R.id.SongSetupLevelNameView);
        LevelNameInput = findViewById(R.id.SongSetupLevelNameInput);

        BPMText.setVisibility(View.VISIBLE);
        BPMInput.setVisibility(View.VISIBLE);
        LengthText.setVisibility(View.VISIBLE);
        LengthInput.setVisibility(View.VISIBLE);
        LevelName.setVisibility(View.VISIBLE);
        LevelNameInput.setVisibility(View.VISIBLE);

        SongSelector.setEnabled(false);
        CheckMetadata();
    }

    /**
     * Searches Music or Downloads folder for MP3's and sets selection spinner to files
     */
    private void SearchFolder() {
        String Choice = LocationSelector.getSelectedItem().toString();
        String Path;
        if (Choice.equals("Music")) {
            Path = Environment.DIRECTORY_MUSIC;
        } else {
            Path = Environment.DIRECTORY_DOWNLOADS;
        }
        Directory = Environment.getExternalStoragePublicDirectory(Path);
        String[] AllSongs = Directory.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".mp3");
            }
        });
        if (AllSongs == null || AllSongs.length == 0) {
            ShowError("No Songs Found");
            State = 0;
        } else {
            List<String> Options = Arrays.asList(AllSongs);
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Options);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            SongSelector = findViewById(R.id.SongSetupMP3);
            SongSelector.setAdapter(adapter);
            TextView SelectorView = findViewById(R.id.MP3LocationView);
            SelectorView.setVisibility(View.VISIBLE);
            SongSelector.setVisibility(View.VISIBLE);
            LocationSelector.setEnabled(false);
        }
    }

    /**
     * Checks metadata of file to get title, author and length
     */
    private void CheckMetadata() {
        PickedSong = new File(Directory, SelectedMP3);
        MediaMetadataRetriever Checker = new MediaMetadataRetriever();
        Checker.setDataSource(PickedSong.getPath());
        Artist = Checker.extractMetadata(MediaMetadataRetriever.METADATA_KEY_AUTHOR);
        String FullLength = Checker.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        SongName = Checker.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);

        Length = Integer.parseInt(FullLength);
        Length = Length / 1000;

        TextView ArtistView = findViewById(R.id.SongAuthorView);
        TextView FullLengthView = findViewById(R.id.SongTrueLengthView);
        TextView TitleView = findViewById(R.id.SongTitleView);
        TextView MetadataView = findViewById(R.id.SongDetailsView);
        TextView T1 = findViewById(R.id.SongTitleTextView);
        TextView T2 = findViewById(R.id.SongAuthorTextView);
        TextView T3 = findViewById(R.id.SongLengthTextView);

        T1.setVisibility(View.VISIBLE);
        T2.setVisibility(View.VISIBLE);
        T3.setVisibility(View.VISIBLE);

        ArtistView.setVisibility(View.VISIBLE);
        ArtistView.setText(Artist);
        FullLengthView.setVisibility(View.VISIBLE);
        FullLengthView.setText("" + Length);
        TitleView.setVisibility(View.VISIBLE);
        TitleView.setText(SongName);
        MetadataView.setVisibility(View.VISIBLE);
        ArtistView.setText(SelectedMP3);
    }

    @Override
    public void onBackPressed() {
        Intent I = new Intent();
        I.putExtra("What", 0);
        setResult(1, I);
        finish();
    }
}

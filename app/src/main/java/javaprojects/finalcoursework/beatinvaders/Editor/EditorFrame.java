package javaprojects.finalcoursework.beatinvaders.Editor;

import android.content.Context;
import android.graphics.Canvas;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;

/**
 * Object for hold information for a single frame of the editor
 */
public class EditorFrame {

    int FrameNumber;
    ArrayList<EditorStamp> Stamps;

    EditorStamp SavedStamp;
    EditorStamp CopiedStamp;

    /**
     * Takes in frame number and creates an empty list of Stamp objects
     * @param Number
     */
    public EditorFrame(int Number){
        FrameNumber = Number;
        Stamps = new ArrayList<>();
    }

    public void AddStamp(EditorStamp NewStamp){
        Stamps.add(NewStamp);
    }

    /**
     * Takes stamp template and creates a new stamp with given details and click location
     * @param NewStamp Stamp details
     * @param ClickLocation Location of click
     */
    public void AddStamp(EditorStampTemplate NewStamp, Point ClickLocation){
        EditorStamp New = new EditorStamp(FrameNumber, ClickLocation,NewStamp);
        Stamps.add(New);
    }

    public void RemoveStamp(int Index){
        Stamps.remove(Index);
    }

    /**
     * Gets stamp at the location of click
     * @param clickPoint Location where clicked
     * @return Stamp that was clicked, or null if nothing was clicked
     */
    private EditorStamp GetStamp(Point clickPoint) {
        for(EditorStamp CurrentStamp : Stamps){
            if(CurrentStamp.CheckClick(clickPoint)){
                return CurrentStamp;
            }
        }
        return null;
    }

    /**
     * Draws all stamps onto canvas according to scales provided
     * @param canvas
     * @param C
     * @param XScale
     * @param YScale
     */
    public void Draw(Canvas canvas, Context C, int XScale, int YScale) {
        for(EditorStamp Stamp: Stamps){
            Stamp.Draw(canvas, C, XScale,YScale);
        }
    }

    /**
     * Moves saved stamp reference to clicked location
     * @param ClickLocation Location of click
     */
    public void MoveStamp(Point ClickLocation) {
            SavedStamp.Location = ClickLocation;
    }

    /**
     * Selects a stamp
     * @param ClickLocation
     * @return
     */
    public boolean SelectStamp(Point ClickLocation) {
        SavedStamp = GetStamp(ClickLocation);
        if(SavedStamp == null){
            return false;
        }
        return true;
    }

    /**
     * Checks list for SavedStamp and deleted.
     * Saved stamp is set when clicked
     */
    public void DeleteSelected() {
        Stamps.remove(SavedStamp);
    }

    /**
     *
     */
    public boolean CopyStamp(Point convertedClick) {
        CopiedStamp = GetStamp(convertedClick);
        if(CopiedStamp == null){
            return false;
        }else{
            return true;
        }

    }

    /**
     * Adds copied stamp to frame at location given
     * @param copiedStamp editor copied stamp
     * @param convertedClick location clicked to paste
     */
    public void PasteStamp(EditorStamp copiedStamp, Point convertedClick) {
        CopiedStamp = new EditorStamp(copiedStamp);
        CopiedStamp.BeatNumber = FrameNumber;
        CopiedStamp.Location = convertedClick;
        Stamps.add(CopiedStamp);
    }

    /**
     * Writes information about frame objects into writers
     * @param bullets
     * @param enemies
     * @param spawners
     * @param events
     * @throws IOException
     */
    public void WriteToFiles(BufferedWriter bullets, BufferedWriter enemies, BufferedWriter spawners, BufferedWriter events) throws IOException {
        for(EditorStamp Stamp : Stamps){
            switch(Stamp.Details.Type){
                case 1:{
                    Stamp.WriteToFile(bullets);
                    break;
                }
                case 2:{
                    Stamp.WriteToFile(enemies);
                    break;
                }
                case 3:{
                    Stamp.WriteToFile(spawners);
                    break;
                }
                case 4:{
                    Stamp.WriteToFile(events);
                }
            }
        }
    }
}

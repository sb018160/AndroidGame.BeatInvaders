package javaprojects.finalcoursework.beatinvaders.Editor;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;

import javaprojects.finalcoursework.beatinvaders.Editor.Views.EditorCanvasView;
import javaprojects.finalcoursework.beatinvaders.Song.Song;
import javaprojects.finalcoursework.beatinvaders.R;
import javaprojects.finalcoursework.beatinvaders.Song.SongPlayer;

/**
 * Editor activity, holds all functions for the editor UI to object communication
 * and error checking when creating objects
 */
public class EditorActivity extends AppCompatActivity {
    Context C;

    EditorCanvasView EditorCanvas;

    Song EditedSong;
    Editor LevelEditor;


    TextView SongNameView;
    TextView BPMView;
    TextView StateView;

    SongPlayer Player;

    int AddObjectID;
    String AddObjectSelection;

    EditorStampTemplate EditorAddedObject;
    PairedBullet Pair;

    ImageButton Play;

    Thread SongPlayerThread;
    volatile boolean ThreadRunning;

    boolean SavedInputs = false;
    String[] BoxInputs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        C = getBaseContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editorbase);

        EditorCanvas = findViewById(R.id.EditorCanvasView);

        try {
            GetSong();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
        SetUpThread();
        SetUpEditor();
        SetUpNavigator();
        SetUpCanvas();
        SetUpOptions();
        UpdateDetails();
    }

    /**
     * Gets song information about the song from the data passed in the Intent when activity was created.
     *
     * @throws IOException
     */
    private void GetSong() throws IOException {
        Intent Data = getIntent();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        if (Data.getBooleanExtra("Flag", false)) { //If an existing level
            String SongName = Data.getStringExtra("SongName");
            String LevelName = Data.getStringExtra("LevelName");
            SetUpExistingSong(SongName, LevelName);
        } else {//Else, new level needs to be set up, all files need to be created.
            Bundle NewSongData = Data.getBundleExtra("Data");
            EditedSong = new Song(NewSongData.getString("SongName"), NewSongData.getString("SongAuthor"), NewSongData.getInt("SongBPM"), NewSongData.getInt("SongLength"), NewSongData.getString("SongPath"), NewSongData.getString("LevelName"));
            try {
                CreateFiles();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    /**
     * Sets up the root view of the activity.
     * Called when going back to root view
     */
    private void SetBaseLayout() {
        setContentView(R.layout.activity_editorbase);
        SetUpNavigator();
        SetUpCanvas();
        SetUpOptions();
        UpdateDetails();
    }

    /**
     * Sets up editor and song player
     */
    private void SetUpEditor() {
        if (LevelEditor == null) {//If level editor wasn't set up by getting an existing song
            LevelEditor = new Editor(EditedSong); //Create new level editor
        }
        Player = new SongPlayer();
        try {
            Player.SetSong(EditedSong); //Set mediaplayer to play the song that is being edited
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Creates all required .txt files if the song is new
     *
     * @throws IOException
     */
    private void CreateFiles() throws IOException {
        File SongDir = Environment.getExternalStorageDirectory();
        SongDir = new File(SongDir, "Beat Invaders");
        SongDir = new File(SongDir, "Songs");
        SongDir = new File(SongDir, EditedSong.Title);
        if (!SongDir.exists()) {
            SongDir.mkdirs();
        }
        SongDir = new File(SongDir, EditedSong.LevelName);
        SongDir.mkdirs();

        InputStream SongStream = new FileInputStream(new File(EditedSong.MP3Path));
        File MP3Dir = new File(SongDir, EditedSong.Title + ".mp3");
        CopyStreams(SongStream, new FileOutputStream(MP3Dir));
        EditedSong.MP3Path = MP3Dir.getPath();

        File SongInfo = new File(SongDir, "SongInfo.txt");
        SongInfo.createNewFile();

        File RequiredFile;
        RequiredFile = new File(SongDir, "EventsFile.txt");
        RequiredFile.createNewFile();
        RequiredFile = new File(SongDir, "BulletsFile.txt");
        RequiredFile.createNewFile();
        RequiredFile = new File(SongDir, "SpawnerFile.txt");
        RequiredFile.createNewFile();
        RequiredFile = new File(SongDir, "EnemiesFile.txt");
        RequiredFile.createNewFile();

        WriteSongInfo(SongInfo);
    }

    /**
     * Writes to SongInfo.txt details about the song object
     *
     * @param Directory
     * @throws IOException
     */
    private void WriteSongInfo(File Directory) throws IOException {
        String fileContent = EditedSong.ToString();
        BufferedWriter writer = new BufferedWriter(new FileWriter(Directory));
        writer.write(fileContent);
        writer.close();
    }

    /**
     * Used for copying mp3's from the original new song directory to the game directory.
     *
     * @param istr
     * @param ostr
     * @throws IOException
     */
    private void CopyStreams(InputStream istr, OutputStream ostr) throws IOException {
        byte[] buffer = new byte[1024];
        int length;
        while ((length = istr.read(buffer)) > 0) {
            ostr.write(buffer, 0, length);
        }
        ostr.flush();
        ostr.close();
        istr.close();
    }

    /**
     * If editing an existing song, then information from files needs to be read and loaded into the editor
     *
     * @param SongName  Name of song
     * @param LevelName Name of level within the song
     * @throws IOException
     */
    private void SetUpExistingSong(String SongName, String LevelName) throws IOException {
        File SongDir = Environment.getExternalStorageDirectory();
        SongDir = new File(SongDir, "Beat Invaders");
        SongDir = new File(SongDir, "Songs");
        SongDir = new File(SongDir, SongName);
        SongDir = new File(SongDir, LevelName);
        FileReader fileReader = new FileReader(new File(SongDir, "SongInfo.txt"));
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        EditedSong = new Song(bufferedReader.readLine());
        bufferedReader.close();
        SetUpExistingLevel();
    }

    /**
     * Sets up options buttons at the botton of root editor layout
     */
    private void SetUpOptions() {
        Button MoveButton = findViewById(R.id.OptionsButtonMove);
        Button CopyButton = findViewById(R.id.OptionsButtonCopy);
        Button PasteButton = findViewById(R.id.OptionsButtonPaste);
        Button DeleteButton = findViewById(R.id.OptionsButtonDelete);
        Button AddButton = findViewById(R.id.OptionsButtonAdd);

        MoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelEditor.ChangeState(1);
                UpdateDetails();
            }
        });
        CopyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelEditor.ChangeState(2);
                UpdateDetails();
            }
        });
        PasteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelEditor.ChangeState(3);
                UpdateDetails();
            }
        });
        DeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelEditor.ChangeState(4);
                UpdateDetails();
            }
        });
        AddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoreOptions();
            }
        });
    }

    /**
     * Sets up text views and buttons at the top of root editor layout
     */
    private void SetUpNavigator() {
        SongNameView = findViewById(R.id.NavigatorSongNameView);
        SongNameView.setText(EditedSong.Title);
        BPMView = findViewById(R.id.NavigatorMaxBeatView);
        StateView = findViewById(R.id.NavigatorTouchStateView);
        BPMView.setText(LevelEditor.getCurrentBeat() + "/" + LevelEditor.getMaxFrame());

        Play = findViewById(R.id.NavigatorButtonPlay);
        ImageButton Back = findViewById(R.id.NavigatorButtonBack);
        ImageButton Next = findViewById(R.id.NavigatorButtonNext);
        ImageButton Forward = findViewById(R.id.NavigatorButtonForward);
        ImageButton Rewind = findViewById(R.id.NavigatorButtonRewind);

        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ThreadRunning) {
                    Play.setImageDrawable(getResources().getDrawable(R.drawable.button_pause));
                    StartPlayerThread();
                } else {
                    StopPlayerThread();
                    Play.setImageDrawable(getResources().getDrawable(R.drawable.button_play));
                }
            }
        });
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelEditor.ChangeFrame(-1);
                UpdateDetails();
            }
        });
        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelEditor.ChangeFrame(1);
                UpdateDetails();
            }
        });
        Forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelEditor.ChangeFrame(10);
                UpdateDetails();
            }
        });
        Rewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LevelEditor.ChangeFrame(-10);
                UpdateDetails();
            }
        });
    }

    /**
     * Updates details in the textviews
     */
    private void UpdateDetails() {
        StateView.setText(LevelEditor.StateToString());
        BPMView.setText(LevelEditor.getCurrentBeat() + "/" + LevelEditor.getMaxFrame());
        if (!EditorCanvas.WaitingToDraw) {
            Editor.Updated = false;
            EditorCanvas.WaitingToDraw = true;
            EditorCanvas.invalidate();
        }
    }

    /**
     * Sets up music player thread,
     * This increments the editor frames based on the position in the song.
     */
    private void SetUpThread() {
        SongPlayerThread = new Thread() {
            public void run() {
                while (ThreadRunning) {
                    UpdateEditor();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (Editor.Updated) {
                                UpdateDetails();
                            }
                        }
                    });
                }
            }
        };
        SongPlayerThread.start();
    }

    /**
     * Updates editor according to time in the song player,
     * Called by SongPlayerThread
     */
    private void UpdateEditor() {
        double MiliSeconds = Player.GetTime();
        double Seconds = (MiliSeconds / 1000.0);
        //If current position in song is longer than map length, stop song
        if (Seconds > EditedSong.LengthTotalSeconds) {
            StopPlayerThread();
        } else {
            if (LevelEditor.CurrentTime < Seconds) {
                LevelEditor.SetTime(Seconds);
            }
        }
    }

    /**
     * Starts the thread and causes the song player to jump to current frame
     */
    private void StartPlayerThread() {
        ThreadRunning = true;
        Player.SeekTo((int) ((LevelEditor.CurrentTime * 1000) + 0.5));

        SetUpThread();
    }

    /**
     * Stops the thread and song player
     */
    private void StopPlayerThread() {
        ThreadRunning = false;
        Player.PauseSong();
    }

    /**
     * Sets up the view by passing all required references
     */
    private void SetUpCanvas() {
        EditorCanvas = findViewById(R.id.EditorCanvasView);
        EditorCanvas.PassEditor(LevelEditor);
    }

    /**
     * Sets up more options view, with buttons for adding new objects and getting more options
     */
    private void MoreOptions() {
        StopPlayerThread();
        setContentView(R.layout.editor_addobjectmenu);
        ImageButton Bullet, Enemy, Spawner, Event, Options, Back;
        Bullet = findViewById(R.id.AddBulletButton);
        Enemy = findViewById(R.id.AddEnemyButton);
        Spawner = findViewById(R.id.AddSpawnerButton);
        Event = findViewById(R.id.AddEventButton);
        Options = findViewById(R.id.CheckOptionsButton);
        Back = findViewById(R.id.GoBackButton);

        Bullet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddObjectMenu(1);
            }
        });
        Enemy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddObjectMenu(2);
            }
        });
        Spawner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddObjectMenu(3);
            }
        });
        Event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddObjectMenu(4);
            }
        });
        Options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetUpEditorOptions();
            }
        });
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetBaseLayout();
            }
        });
    }

    /**
     * Sets up menu for adding specific object types. Changes text and visibility based on what type of object
     * and the subtype.
     *
     * @param ID of object to be added
     */
    private void AddObjectMenu(int ID) {
        setContentView(R.layout.editor_objectparametersmenu);
        AddObjectID = ID;
        TextView TitleView = findViewById(R.id.ObjectNameView);
        TextView ObjectTypeView = findViewById(R.id.ObjectText1);
        Spinner ObjectTypeSelector = findViewById(R.id.ObjectTypeSelector);
        TextView ObjectInfoView1 = findViewById(R.id.ObjectText2);
        final TextView ObjectInfoInput1 = findViewById(R.id.ObjectEditInput1);
        TextView ObjectInfoView2 = findViewById(R.id.ObjectText3);
        final TextView ObjectInfoInput2 = findViewById(R.id.ObjectEditInput2);
        TextView ObjectInfoView3 = findViewById(R.id.ObjectText4);
        TextView ObjectInfoInput3 = findViewById(R.id.ObjectEditInput3);
        TextView ObjectInfoView4 = findViewById(R.id.ObjectText5);
        TextView ObjectInfoInput4 = findViewById(R.id.ObjectEditInput4);
        TextView ObjectInfoInput5 = findViewById(R.id.ObjectEditInput5);
        TextView ObjectInfoInput6 = findViewById(R.id.ObjectEditInput6);

        Button SaveButton = findViewById(R.id.ObjectSaveButton);
        Button CancelButton = findViewById(R.id.ObjectCancelButton);
        Button SetBullet = findViewById(R.id.ObjectSetBullet);

        switch (ID) {
            case 1: {//Bullet
                TitleView.setText("Add Bullet");
                ObjectTypeView.setText("Bullet Type");
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Bullet_Types, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                ObjectTypeSelector.setAdapter(adapter);
                ObjectInfoView1.setText("Speed");
                ObjectInfoView1.setVisibility(View.VISIBLE);
                ObjectInfoInput1.setVisibility(View.VISIBLE);

                ObjectInfoView2.setText("Size");
                ObjectInfoView2.setVisibility(View.VISIBLE);
                ObjectInfoInput2.setVisibility(View.VISIBLE);

                ObjectInfoView3.setText("Rotation");
                ObjectInfoView3.setVisibility(View.VISIBLE);
                ObjectInfoInput3.setVisibility(View.VISIBLE);
                break;
            }
            case 2: {//Enemy
                TitleView.setText("Add Enemy");
                ObjectTypeView.setText("Enemy Type");
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Enemy_Types, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                ObjectTypeSelector.setAdapter(adapter);
                ObjectInfoView1.setText("Speed");
                ObjectInfoView1.setVisibility(View.VISIBLE);
                ObjectInfoInput1.setVisibility(View.VISIBLE);

                ObjectInfoView2.setText("Size");
                ObjectInfoView2.setVisibility(View.VISIBLE);
                ObjectInfoInput2.setVisibility(View.VISIBLE);

                ObjectInfoView3.setText("Health");
                ObjectInfoView3.setVisibility(View.VISIBLE);
                ObjectInfoInput3.setVisibility(View.VISIBLE);

                ObjectInfoView4.setText("Cooldown");
                ObjectInfoView4.setVisibility(View.VISIBLE);
                ObjectInfoInput4.setVisibility(View.VISIBLE);

                SetBullet.setVisibility(View.VISIBLE);
                break;
            }
            case 3: {//Spawner
                TitleView.setText("Add Spawner");
                ObjectTypeView.setText("Spawner Type");
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Spawner_Types, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                ObjectTypeSelector.setAdapter(adapter);
                ObjectInfoView1.setText("Length");
                ObjectInfoView1.setVisibility(View.VISIBLE);
                ObjectInfoInput1.setVisibility(View.VISIBLE);

                ObjectInfoView2.setText("Cooldown Whole");
                ObjectInfoView2.setVisibility(View.VISIBLE);
                ObjectInfoInput2.setVisibility(View.VISIBLE);

                ObjectInfoView3.setText("Cooldown Fraction");
                ObjectInfoView3.setVisibility(View.VISIBLE);
                ObjectInfoInput3.setVisibility(View.VISIBLE);

                SetBullet.setVisibility(View.VISIBLE);
                break;
            }
            case 4: {//Event
                TitleView.setText("Add Event");
                ObjectTypeView.setText("Event Type");
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Event_Types, android.R.layout.simple_spinner_item);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                ObjectTypeSelector.setAdapter(adapter);
                ObjectInfoView1.setText("Length");
                ObjectInfoView1.setVisibility(View.VISIBLE);
                ObjectInfoInput1.setVisibility(View.VISIBLE);

                ObjectInfoView2.setText("Cooldown Whole");
                ObjectInfoView2.setVisibility(View.VISIBLE);
                ObjectInfoInput2.setVisibility(View.VISIBLE);

                ObjectInfoView3.setText("Cooldown Fraction");
                ObjectInfoView3.setVisibility(View.VISIBLE);
                ObjectInfoInput3.setVisibility(View.VISIBLE);

                ObjectInfoView4.setText("Rotation");
                ObjectInfoView4.setVisibility(View.VISIBLE);
                ObjectInfoInput4.setVisibility(View.VISIBLE);
                break;
            }
        }


        if (SavedInputs) {
            ObjectTypeSelector.setSelection(Integer.parseInt(BoxInputs[0]));
            ObjectInfoInput1.setText(BoxInputs[1]);
            ObjectInfoInput2.setText(BoxInputs[2]);
            ObjectInfoInput3.setText(BoxInputs[3]);
            ObjectInfoInput4.setText(BoxInputs[4]);
            ObjectInfoInput5.setText(BoxInputs[5]);
            ObjectInfoInput6.setText(BoxInputs[6]);
        }

        ObjectTypeSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ChangeOptions(AddObjectID, parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SetBullet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveInputs();
                SetBulletPair(AddObjectID);
            }
        });

        CancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoreOptions();
            }
        });

        SaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckSetObjectInputs();
            }
        });
    }

    /**
     * Saves what was already inputted if setBullet is clicked
     */
    private void SaveInputs() {
        Spinner ObjectTypeSelector = findViewById(R.id.ObjectTypeSelector);
        TextView ObjectInfoInput1 = findViewById(R.id.ObjectEditInput1);
        TextView ObjectInfoInput2 = findViewById(R.id.ObjectEditInput2);
        TextView ObjectInfoInput3 = findViewById(R.id.ObjectEditInput3);
        TextView ObjectInfoInput4 = findViewById(R.id.ObjectEditInput4);
        TextView ObjectInfoInput5 = findViewById(R.id.ObjectEditInput5);
        TextView ObjectInfoInput6 = findViewById(R.id.ObjectEditInput6);
        int SelectionPosition = ObjectTypeSelector.getSelectedItemPosition();
        String Param1 = ObjectInfoInput1.getText().toString();
        String Param2 = ObjectInfoInput2.getText().toString();
        String Param3 = ObjectInfoInput3.getText().toString();
        String Param4 = ObjectInfoInput4.getText().toString();
        String Param5 = ObjectInfoInput5.getText().toString();
        String Param6 = ObjectInfoInput6.getText().toString();

        SavedInputs = true;
        BoxInputs = new String[]{"" + SelectionPosition, Param1, Param2, Param3, Param4, Param5, Param6};
    }

    /**
     * Checks to see if any required fields are empty and informs user, otherwise saves object and sets state
     */
    private void CheckSetObjectInputs() {
        Spinner ObjectTypeSelector = findViewById(R.id.ObjectTypeSelector);
        TextView ObjectInfoInput1 = findViewById(R.id.ObjectEditInput1);
        TextView ObjectInfoInput2 = findViewById(R.id.ObjectEditInput2);
        TextView ObjectInfoInput3 = findViewById(R.id.ObjectEditInput3);
        TextView ObjectInfoInput4 = findViewById(R.id.ObjectEditInput4);
        TextView ObjectInfoInput5 = findViewById(R.id.ObjectEditInput5);
        TextView ObjectInfoInput6 = findViewById(R.id.ObjectEditInput6);

        String Selection = ObjectTypeSelector.getSelectedItem().toString();
        String Param1 = "Null", Param2 = "Null", Param3 = "Null", Param4 = "0", Param5 = "0", Param6 = "0";


        Param1 = ObjectInfoInput1.getText().toString();
        Param2 = ObjectInfoInput2.getText().toString();
        Param3 = ObjectInfoInput3.getText().toString();


        switch (AddObjectID) {
            case 1: {//bullet
                switch (Selection) {
                    case "Normal": {
                        break;
                    }
                    case "Circle": {
                        Param4 = ObjectInfoInput4.getText().toString();
                        break;
                    }
                    case "Flower": {
                        Param4 = ObjectInfoInput4.getText().toString();
                        Param5 = ObjectInfoInput5.getText().toString();
                        Param6 = ObjectInfoInput6.getText().toString();
                        break;
                    }
                    case "Spread": {
                        Param4 = ObjectInfoInput4.getText().toString();
                        Param5 = ObjectInfoInput5.getText().toString();
                        Param6 = ObjectInfoInput6.getText().toString();
                        break;
                    }
                }
                break;
            }
            case 2: {//Enemy
                Param4 = ObjectInfoInput4.getText().toString();
                break;
            }
            case 3: {//Spawner
                switch (Selection) {
                    case "Normal": {

                        break;
                    }
                    case "Rotation": {
                        Param4 = ObjectInfoInput4.getText().toString();
                        break;
                    }
                    case "Offset": {
                        Param4 = ObjectInfoInput4.getText().toString();
                        Param5 = ObjectInfoInput5.getText().toString();
                        break;
                    }
                }
                break;
            }
        }
        if (AddObjectID == 2 || AddObjectID == 3) {
            if (Pair == null) {
                TextView ErrorBox = findViewById(R.id.ObjectErrorView);
                ErrorBox.setVisibility(View.VISIBLE);
                ErrorBox.setText("Bullet not set, please fix");
                return;
            }
        }

        if (Param1.equals("") || Param2.equals("") || Param3.equals("") || Param4.equals("") || Param5.equals("") || Param6.equals("")) {
            TextView ErrorBox = findViewById(R.id.ObjectErrorView);
            ErrorBox.setVisibility(View.VISIBLE);
            ErrorBox.setText("Value is empty, please fix");
        } else {
            String[] Parameters = {Selection, Param1, Param2, Param3, Param4, Param5, Param6};
            EditorAddedObject = new EditorStampTemplate(AddObjectID, Parameters);
            if (AddObjectID == 2 || AddObjectID == 3) {
                EditorAddedObject.SetBulletDetails(Pair);
            }
            LevelEditor.SetStamp(EditorAddedObject);
            Pair = null;
            SavedInputs = false;
            SetBaseLayout();
        }
    }

    /**
     * Sets up menu for setting up paired bullet for enemies/spawners
     *
     * @param ID of object, either enemy or spawner
     */
    private void SetBulletPair(int ID) {
        setContentView(R.layout.editor_addbulletmenu);
        if (ID == 2) {
            TextView HomingView = findViewById(R.id.AddBulletHomingView);
            Switch HomingSwitch = findViewById(R.id.AddBulletHomingSwitch);
            HomingView.setVisibility(View.VISIBLE);
            HomingSwitch.setVisibility(View.VISIBLE);
        }
        Spinner BulletTypeSpinner = findViewById(R.id.AddBulletTypeSelector);
        BulletTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ChangeBulletOptions(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button BulletSave = findViewById(R.id.AddBulletSave);
        Button BulletCancel = findViewById(R.id.AddBulletCancel);

        BulletSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBulletPair(AddObjectID);
            }
        });

        BulletCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddObjectMenu(AddObjectID);
            }
        });
    }

    /**
     * Checks to see if any required fields are empty and informs user, otherwise saves object and sets state
     *
     * @param ID of object, either enemy or spawner
     */
    private void CheckBulletPair(int ID) {
        Spinner BulletTypeSpinner = findViewById(R.id.AddBulletTypeSelector);
        String BulletType = BulletTypeSpinner.getSelectedItem().toString();
        Boolean Switch = false;
        if (ID == 2) {
            Switch HomingSwitch = findViewById(R.id.AddBulletHomingSwitch);
            Switch = HomingSwitch.isChecked();
        }
        TextView SpeedInput = findViewById(R.id.AddBulletSpeedInput);
        String BulletSpeed = SpeedInput.getText().toString();
        TextView SizeInput = findViewById(R.id.AddBulletSizeInput);
        String BulletSize = SizeInput.getText().toString();
        TextView RotationInput = findViewById(R.id.AddBulletRotationInput);
        String BulletRotation = RotationInput.getText().toString();
        String BulletExtra1 = "0", BulletExtra2 = "0", BulletExtra3 = "0";
        switch (BulletType) {
            case "Normal": {
                break;
            }
            case "Circle": {
                TextView NumberOfCircles = findViewById(R.id.AddBulletExtra1Input);
                BulletExtra1 = NumberOfCircles.getText().toString();
                break;
            }
            case "Flower": {
                TextView NumberOfCircles = findViewById(R.id.AddBulletExtra1Input);
                BulletExtra1 = NumberOfCircles.getText().toString();
                TextView NumberOfLayers = findViewById(R.id.AddBulletExtra2Input);
                BulletExtra2 = NumberOfLayers.getText().toString();
                TextView LayerSpread = findViewById(R.id.AddBulletExtra3Input);
                BulletExtra3 = LayerSpread.getText().toString();
                break;
            }
            case "Spread": {
                TextView NumberOfBullets = findViewById(R.id.AddBulletExtra1Input);
                BulletExtra1 = NumberOfBullets.getText().toString();
                TextView SpreadAngle = findViewById(R.id.AddBulletExtra2Input);
                BulletExtra2 = SpreadAngle.getText().toString();
                TextView SpeedVariance = findViewById(R.id.AddBulletExtra3Input);
                BulletExtra3 = SpeedVariance.getText().toString();
                break;
            }
        }

        if (BulletSpeed.equals("") || BulletSize.equals("") || BulletRotation.equals("") || BulletExtra1.equals("") || BulletExtra2.equals("") || BulletExtra3.equals("")) {
            TextView ErrorBox = findViewById(R.id.AddBulletErrorView);
            ErrorBox.setText("Value is empty, please fix");
        } else {
            String[] Parameters = {BulletType, BulletSpeed, BulletSize, BulletRotation, BulletExtra1, BulletExtra2, BulletExtra3};
            if (AddObjectID == 2) {
                String Extra;
                if (Switch) {
                    Extra = "1";
                } else {
                    Extra = "0";
                }
                Pair = new PairedBullet(Parameters, true, Extra);
            } else {
                Pair = new PairedBullet(Parameters, false, "");
            }
            AddObjectMenu(AddObjectID);
        }
    }

    /**
     * Changes text views based on what is selected in the spinner
     *
     * @param Selection Spinner selection
     */
    private void ChangeBulletOptions(String Selection) {
        TextView BulletExtra1 = findViewById(R.id.AddBulletExtra1View);
        TextView BulletExtra2 = findViewById(R.id.AddBulletExtra2View);
        TextView BulletExtra3 = findViewById(R.id.AddBulletExtra3View);
        TextView BulletExtra1Input = findViewById(R.id.AddBulletExtra1Input);
        TextView BulletExtra2Input = findViewById(R.id.AddBulletExtra2Input);
        TextView BulletExtra3Input = findViewById(R.id.AddBulletExtra3Input);

        switch (Selection) {
            case "Normal": {
                BulletExtra1.setVisibility(View.INVISIBLE);
                BulletExtra1Input.setVisibility(View.INVISIBLE);
                BulletExtra2.setVisibility(View.INVISIBLE);
                BulletExtra2Input.setVisibility(View.INVISIBLE);
                BulletExtra3.setVisibility(View.INVISIBLE);
                BulletExtra3Input.setVisibility(View.INVISIBLE);
                break;
            }
            case "Circle": {
                BulletExtra1.setText("Number Of Bullets");
                BulletExtra1.setVisibility(View.VISIBLE);
                BulletExtra1Input.setVisibility(View.VISIBLE);
                BulletExtra2.setVisibility(View.INVISIBLE);
                BulletExtra2Input.setVisibility(View.INVISIBLE);
                BulletExtra3.setVisibility(View.INVISIBLE);
                BulletExtra3Input.setVisibility(View.INVISIBLE);
                break;
            }
            case "Flower": {
                BulletExtra1.setText("Number per layer");
                BulletExtra1.setVisibility(View.VISIBLE);
                BulletExtra1Input.setVisibility(View.VISIBLE);
                BulletExtra2.setText("Layers");
                BulletExtra2.setVisibility(View.VISIBLE);
                BulletExtra2Input.setVisibility(View.VISIBLE);
                BulletExtra3.setText("Angle between layer");
                BulletExtra3.setVisibility(View.VISIBLE);
                BulletExtra3Input.setVisibility(View.VISIBLE);
                break;
            }
            case "Spread": {
                BulletExtra1.setText("Number Of Bullets");
                BulletExtra1.setVisibility(View.VISIBLE);
                BulletExtra1Input.setVisibility(View.VISIBLE);
                BulletExtra2.setText("Spread angle");
                BulletExtra2.setVisibility(View.VISIBLE);
                BulletExtra2Input.setVisibility(View.VISIBLE);
                BulletExtra3.setText("Speed Variation");
                BulletExtra3.setVisibility(View.VISIBLE);
                BulletExtra3Input.setVisibility(View.VISIBLE);
                break;
            }
        }
    }

    /**
     * Changes what options are shown in the textviews for adding an object,
     * based on what object type and subtype is selected
     *
     * @param ID        of object spawn
     * @param Selection spinner selection
     */
    private void ChangeOptions(int ID, String Selection) {
        AddObjectSelection = Selection;
        TextView ObjectInfoView4 = findViewById(R.id.ObjectText5);
        TextView ObjectInfoInput4 = findViewById(R.id.ObjectEditInput4);
        TextView ObjectInfoView5 = findViewById(R.id.ObjectText6);
        TextView ObjectInfoInput5 = findViewById(R.id.ObjectEditInput5);
        TextView ObjectInfoView6 = findViewById(R.id.ObjectText7);
        TextView ObjectInfoInput6 = findViewById(R.id.ObjectEditInput6);
        TextView ObjectInfoView7 = findViewById(R.id.ObjectText8);
        TextView ObjectInfoInput7 = findViewById(R.id.ObjectEditInput7);

        switch (ID) {
            case 1: {//bullet
                switch (Selection) {
                    case "Normal": {
                        ObjectInfoView4.setVisibility(View.INVISIBLE);
                        ObjectInfoInput4.setVisibility(View.INVISIBLE);
                        ObjectInfoView5.setVisibility(View.INVISIBLE);
                        ObjectInfoInput5.setVisibility(View.INVISIBLE);
                        ObjectInfoView6.setVisibility(View.INVISIBLE);
                        ObjectInfoInput6.setVisibility(View.INVISIBLE);
                        ObjectInfoView7.setVisibility(View.INVISIBLE);
                        ObjectInfoInput7.setVisibility(View.INVISIBLE);
                        break;
                    }
                    case "Circle": {
                        ObjectInfoView4.setText("Number of bullets");
                        ObjectInfoView4.setVisibility(View.VISIBLE);
                        ObjectInfoInput4.setVisibility(View.VISIBLE);
                        ObjectInfoView5.setVisibility(View.INVISIBLE);
                        ObjectInfoInput5.setVisibility(View.INVISIBLE);
                        ObjectInfoView6.setVisibility(View.INVISIBLE);
                        ObjectInfoInput6.setVisibility(View.INVISIBLE);
                        ObjectInfoView7.setVisibility(View.INVISIBLE);
                        ObjectInfoInput7.setVisibility(View.INVISIBLE);
                        break;
                    }
                    case "Flower": {
                        ObjectInfoView4.setText("Number per layer");
                        ObjectInfoView4.setVisibility(View.VISIBLE);
                        ObjectInfoInput4.setVisibility(View.VISIBLE);
                        ObjectInfoView5.setText("Layers");
                        ObjectInfoView5.setVisibility(View.VISIBLE);
                        ObjectInfoInput5.setVisibility(View.VISIBLE);
                        ObjectInfoView6.setText("Angle between layer");
                        ObjectInfoView6.setVisibility(View.VISIBLE);
                        ObjectInfoInput6.setVisibility(View.VISIBLE);
                        ObjectInfoView7.setVisibility(View.INVISIBLE);
                        ObjectInfoInput7.setVisibility(View.INVISIBLE);
                        break;
                    }
                    case "Spread": {
                        ObjectInfoView4.setText("Number of bullets");
                        ObjectInfoView4.setVisibility(View.VISIBLE);
                        ObjectInfoInput4.setVisibility(View.VISIBLE);
                        ObjectInfoView5.setText("Spread angle");
                        ObjectInfoView5.setVisibility(View.VISIBLE);
                        ObjectInfoInput5.setVisibility(View.VISIBLE);
                        ObjectInfoView6.setText("Speed variation");
                        ObjectInfoView6.setVisibility(View.VISIBLE);
                        ObjectInfoInput6.setVisibility(View.VISIBLE);
                        ObjectInfoView7.setVisibility(View.INVISIBLE);
                        ObjectInfoInput7.setVisibility(View.INVISIBLE);
                        break;
                    }
                }
            }
            case 2: {//enemy
                switch (Selection) {
                    case "Normal": {
                        ObjectInfoView5.setVisibility(View.INVISIBLE);
                        ObjectInfoInput5.setVisibility(View.INVISIBLE);
                        ObjectInfoView6.setVisibility(View.INVISIBLE);
                        ObjectInfoInput6.setVisibility(View.INVISIBLE);
                        ObjectInfoView7.setVisibility(View.INVISIBLE);
                        ObjectInfoInput7.setVisibility(View.INVISIBLE);
                        break;
                    }
                }
            }
            case 3: {//spawner
                switch (Selection) {
                    case "Normal": {
                        ObjectInfoView4.setVisibility(View.INVISIBLE);
                        ObjectInfoInput4.setVisibility(View.INVISIBLE);
                        ObjectInfoView5.setVisibility(View.INVISIBLE);
                        ObjectInfoInput5.setVisibility(View.INVISIBLE);
                        ObjectInfoView6.setVisibility(View.INVISIBLE);
                        ObjectInfoInput6.setVisibility(View.INVISIBLE);
                        ObjectInfoView7.setVisibility(View.INVISIBLE);
                        ObjectInfoInput7.setVisibility(View.INVISIBLE);
                        break;
                    }
                    case "Rotation": {
                        ObjectInfoView4.setText("Rotation");
                        ObjectInfoView4.setVisibility(View.VISIBLE);
                        ObjectInfoInput4.setVisibility(View.VISIBLE);
                        ObjectInfoView5.setVisibility(View.INVISIBLE);
                        ObjectInfoInput5.setVisibility(View.INVISIBLE);
                        ObjectInfoView6.setVisibility(View.INVISIBLE);
                        ObjectInfoInput6.setVisibility(View.INVISIBLE);
                        ObjectInfoView7.setVisibility(View.INVISIBLE);
                        ObjectInfoInput7.setVisibility(View.INVISIBLE);
                        break;
                    }
                    case "Offset": {
                        ObjectInfoView4.setText("Offset X");
                        ObjectInfoView4.setVisibility(View.VISIBLE);
                        ObjectInfoInput4.setVisibility(View.VISIBLE);
                        ObjectInfoView5.setText("Offset Y");
                        ObjectInfoView5.setVisibility(View.VISIBLE);
                        ObjectInfoInput5.setVisibility(View.VISIBLE);
                        ObjectInfoView6.setVisibility(View.INVISIBLE);
                        ObjectInfoInput6.setVisibility(View.INVISIBLE);
                        ObjectInfoView7.setVisibility(View.INVISIBLE);
                        ObjectInfoInput7.setVisibility(View.INVISIBLE);
                        break;
                    }
                }
            }
            case 4: {//event
                switch (Selection) {

                }
            }
        }
    }

    /**
     * Sets up more options view and buttons
     */
    private void SetUpEditorOptions() {
        setContentView(R.layout.editor_options);
        Button Play = findViewById(R.id.EditorOptionsPlaytest);
        Button Save = findViewById(R.id.EditorOptionsSave);
        Button Menu = findViewById(R.id.EditorOptionsBackMenu);
        Button Quit = findViewById(R.id.EditorOptionsQuit);
        Button Back = findViewById(R.id.EditorOptionsBack);

        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveLevel();
                PlayLevel();
            }
        });
        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveLevel();
            }
        });
        Menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent();
                I.putExtra("What", 0);
                setResult(1, I);
                finish();
            }
        });
        Quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent I = new Intent();
                I.putExtra("What", 0);
                setResult(-1, I);
                finish();
            }
        });
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoreOptions();
            }
        });
    }

    /**
     * Saves level and finishes activity with flag that information is coming from editor
     * and information about level
     */
    private void PlayLevel() {
        Intent I = new Intent();
        I.putExtra("SongName", EditedSong.Title);
        I.putExtra("LevelName", EditedSong.LevelName);
        I.putExtra("What", 34);
        setResult(4, I);
        finish();
    }

    /**
     * Saves editor to all required files.
     */
    private void SaveLevel() {
        EditorFileHandler.SaveEditor(LevelEditor.GetFrames(), LevelEditor.EditedSong, C);
    }

    /**
     * Loads information from files for the existing level into the editor
     */
    private void SetUpExistingLevel() {
        LevelEditor = new Editor(EditedSong);
        try {
            EditorFileHandler.LoadLevel(LevelEditor);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Goes back to menu
     */
    public void onBackPressed() {
        Intent I = new Intent();
        I.putExtra("What", 0);
        setResult(1, I);
        finish();
    }
}

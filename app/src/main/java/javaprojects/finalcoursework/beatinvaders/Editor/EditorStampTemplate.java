package javaprojects.finalcoursework.beatinvaders.Editor;

/**
 * Template for a stamp, holds all paremeters for the object.
 */
public class EditorStampTemplate {

    int Type;
    int StampSubType;
    String Arg1,Arg2,Arg3,Arg4,Arg5,Arg6;
    PairedBullet Child;

    /**
     * Takes in Main Type of object and string of details
     * @param Type of object
     * @param Details string[]
     */
    public EditorStampTemplate(int Type, String[] Details){
        this.Type = Type;
        SplitDetails(Details);
    }

    /**
     * Splits given details into individual arguments and type
     * @param details
     */
    private void SplitDetails(String[] details) {
        switch(details[0]){
            case "Normal":{
                StampSubType = 0;
                break;
            }
            case "Simple":{
                StampSubType = 0;
                break;
            }
            case "Complex":{
                StampSubType = 1;
                break;
            }
            case "Circle":{
                StampSubType = 1;
                break;
            }
            case "Rotation":{
                StampSubType = 1;
                break;
            }
            case "Flower":{
                StampSubType = 2;
                break;
            }
            case "Offset":{
                StampSubType = 2;
                break;
            }
            case "Spread":{
                StampSubType = 3;
                break;
            }
            default:{
                StampSubType = Integer.parseInt(details[0]);
                break;
            }
        }
        Arg1 = details[1];
        Arg2 = details[2];
        Arg3 = details[3];
        Arg4 = details[4];
        Arg5 = details[5];
        Arg6 = details[6];
    }

    /**
     * Sets details of the child of the stamp, in the case of a spawner, this is what the spawner will create
     * @param Bullet Spawner spawnee
     */
    public void SetBulletDetails(PairedBullet Bullet){
        Child = Bullet;
    }

    public String ToString(){
        String Construct = "";
        Construct +=","+StampSubType+","+Arg1+","+Arg2+","+Arg3+","+Arg4+","+Arg5+","+Arg6;
        if(Child != null){
            Construct += Child.ToString();
        }
        return Construct;
    }

}

package javaprojects.finalcoursework.beatinvaders.Editor;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import java.io.BufferedWriter;
import java.io.IOException;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;

/**
 * Class holding information about the object an editor frame holds.
 */
public class EditorStamp {

    Point Location;
    EditorStampTemplate Details;
    int BeatNumber;

    /**
     * Takes a template and adds a beat number and location to it.
     * @param BNumber
     * @param Location
     * @param Template
     */
    public EditorStamp(int BNumber, Point Location, EditorStampTemplate Template) {
        this.Location = Location;
        BeatNumber = BNumber;
        Details = Template;
    }

    /**
     * Clones stamp
     * @param Other stamp to clone
     */
    public EditorStamp(EditorStamp Other) {
        this.Location = Other.Location;
        BeatNumber = Other.BeatNumber;
        Details = Other.Details;
    }

    /**
     * Draws stamp on the canvas
     * @param canvas
     * @param C
     * @param XScale
     * @param YScale
     */
    public void Draw(Canvas canvas, Context C, int XScale, int YScale) {
        Point DrawnLocation = new Point(Location.X * XScale, Location.Y * YScale);
        Paint Colour = new Paint();
            RectF StampLocation = new RectF((float) DrawnLocation.X - (XScale / 2), (float) DrawnLocation.Y - (YScale / 2), (float) DrawnLocation.X + XScale / 2, (float) DrawnLocation.Y + (YScale / 2));
            switch (Details.Type) {
                case 1: {//Bullet
                    Colour.setColor(Color.RED);
                    break;
                }
                case 2: {//Enemy
                    Colour.setColor(Color.BLUE);
                    break;
                }
                case 3: {//Spawner
                    Colour.setColor(Color.MAGENTA);
                    break;
                }
                case 4: {
                    Colour.setColor(Color.BLACK);
                    break;
                }
            }
            canvas.drawOval(StampLocation,Colour);
    }

    /**
     * Checks whether any stamps were clicked on
     * @param clickPoint Point on click
     * @return if stamp was clicked
     */
    public boolean CheckClick(Point clickPoint) {
        double dx = Location.X - clickPoint.X;
        double dy = Location.Y - clickPoint.Y;
        double radiusSum = 2;
        if ((dx * dx) + (dy * dy) <= radiusSum * radiusSum) {
            return true;
        }
        return false;
    }

    /**
     * Writes stamps to file given.
     * @param writer
     * @throws IOException
     */
    public void WriteToFile(BufferedWriter writer) throws IOException {
        if(Details.Type != 4){
            String Construct = "";
            Construct += BeatNumber + ","+Location.Xint+","+Location.Yint;
            Construct += Details.ToString();
            writer.write(Construct+"\n");
        }
    }
}

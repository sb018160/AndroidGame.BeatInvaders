package javaprojects.finalcoursework.beatinvaders.Editor.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import javaprojects.finalcoursework.beatinvaders.Editor.Editor;
import javaprojects.finalcoursework.beatinvaders.Extras.Point;

/**
 * View class for the editor, holds reference to the editor object for drawing purposes
 */
public class EditorCanvasView extends View {


    Context C;
    double Height;
    double Width;

    Editor LevelEditor;

    public boolean WaitingToDraw = false;


    public EditorCanvasView(Context context,@Nullable AttributeSet attrs) {
        super(context, attrs);
        C = context;

        setOnTouchListener(new OnTouchListener() { //Will just send point to the editor.
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Point ClickPoint = new Point(event.getX(),event.getY());
                if(LevelEditor.DoClick(ClickPoint)){
                    invalidate();
                }
                return false;
            }
        });
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        // the height will be set at this point
        Height = getMeasuredHeight();
        Width = getMeasuredWidth();
    }

    /**
     * Checks if editor exists before calling .draw on it.
     * @param canvas
     */
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);
        if(LevelEditor != null) {
            LevelEditor.Draw(canvas, C, getMeasuredWidth()-5,getMeasuredHeight()-10);
        }
        WaitingToDraw = false;
    }

    public void PassEditor(Editor E){
        LevelEditor = E;
    }

}

package javaprojects.finalcoursework.beatinvaders.Editor;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Song.Song;

/**
 * Editor object that handles saving, reading and keeping track of all objects in the editor.
 * Holds functions for drawing self as well.
 */
public class Editor {
    final static int ACTION_NULL = 0;
    final static int ACTION_MOVE = 1;
    final static int ACTION_DELETE = 4;
    final static int ACTION_COPY = 2;
    final static int ACTION_PASTE = 3;
    final static int ACTION_EDIT = 5;
    final static int ACTION_CREATESTAMP = 6;
    public static boolean Updated = true;

    boolean StateFlag = false;

    Song EditedSong;

    double TimePerBeat;

    int CanvasSizeX, CanvasSizeY;
    int ScaleX, ScaleY;

    double CurrentTime;
    int MaxTime;
    int CurrentState = 0;
    int CurrentFrame = 0;
    int MaxFrame;

    ArrayList<EditorFrame> AllFrames;
    EditorFrame WorkedOnFrame;

    EditorStampTemplate SavedStamp;
    EditorStamp CopiedStamp;

    /**
     * Construct takes song that is going to be edited, setting up base information from the song
     *
     * @param EditedSong
     */
    public Editor(Song EditedSong) {
        this.EditedSong = EditedSong;
        AllFrames = new ArrayList<>();
        CurrentFrame = 0;
        MaxTime = EditedSong.LengthTotalSeconds;
        CurrentTime = 0;
        MaxFrame = (int) ((((double) MaxTime / 60.0) * EditedSong.SongBPM) + 0.5);
        TimePerBeat = 60.0 / (double) EditedSong.SongBPM;
        AllFrames.ensureCapacity(MaxFrame);
        for (int I = 0; I <= MaxFrame; I++) {
            EditorFrame NewFrame = new EditorFrame(I);
            AllFrames.add(NewFrame);
        }
        WorkedOnFrame = AllFrames.get(0);
    }

    public ArrayList<EditorFrame> GetFrames() {
        return AllFrames;
    }

    public void SetFrameSize(int XSize, int YSize) {
        CanvasSizeX = XSize;
        CanvasSizeY = YSize;

        ScaleX = CanvasSizeX / 50;
        ScaleY = CanvasSizeY / 50;
    }

    public int getMaxFrame() {
        return MaxFrame;
    }

    /**
     * Changes current frame by given number, checks if this will cause an overflow/underflow first.
     * @param ChangeBy
     * @return True whether changed or not.
     */
    public boolean ChangeFrame(int ChangeBy) {
        if (CurrentFrame + ChangeBy <= MaxFrame && CurrentFrame + ChangeBy >= 0) {
            CurrentFrame += ChangeBy;
            CurrentTime = TimePerBeat * (double)CurrentFrame;
            WorkedOnFrame = AllFrames.get(CurrentFrame);
            return true;
        }
        return false;
    }

    /**
     * Changes state of editor, used by buttons
     * @param NewState state to set
     */
    public void ChangeState(int NewState) {
        if (CurrentState == NewState) {
            CurrentState = 0;
        } else {
            CurrentState = NewState;
        }
    }

    /**
     * When adding object, sets the saved stamp to given stamp
     * Sets editor state to "adding" after
     * @param Save
     */
    public void SetStamp(EditorStampTemplate Save) {
        SavedStamp = Save;
        CurrentState = 6;
    }

    public String StateToString() {
        switch (CurrentState) {
            case 0: {
                return "Nothing";
            }
            case 1: {
                return "Move";
            }
            case 2: {
                return "Copy";
            }
            case 3: {
                return "Paste";
            }
            case 4: {
                return "Delete";
            }
            case 5: {
                return "Edit";
            }
            case 6: {
                return "Add object";
            }
        }
        return "Nothing";
    }

    /**
     * Reacts to click accordingly based on point given.
     * Scales point according to the canvas size.
     * @param clickPoint
     * @return true or false whether object was clicked.
     */
    public boolean DoClick(Point clickPoint) {
        Point ConvertedClick = new Point(clickPoint.X / ScaleX, clickPoint.Y / ScaleY);
        switch (CurrentState) {
            case ACTION_NULL: {
                return false;
            }
            case ACTION_MOVE: {
                if (StateFlag) {
                    WorkedOnFrame.MoveStamp(ConvertedClick);
                    CurrentState = 1;
                    StateFlag = false;
                } else {
                    if (WorkedOnFrame.SelectStamp(ConvertedClick)) {
                        StateFlag = true;
                    }
                }
                break;
            }
            case ACTION_DELETE: {
                if (WorkedOnFrame.SelectStamp(ConvertedClick)) {
                    WorkedOnFrame.DeleteSelected();
                }
                break;
            }
            case ACTION_COPY: {
                if (WorkedOnFrame.CopyStamp(ConvertedClick)) {
                    CopiedStamp = WorkedOnFrame.CopiedStamp;
                    CurrentState = ACTION_PASTE;
                }
                break;
            }
            case ACTION_PASTE: {
                if (CopiedStamp != null) {
                    WorkedOnFrame.PasteStamp(CopiedStamp, ConvertedClick);
                }
                break;
            }
            case ACTION_EDIT: {

                break;
            }
            case ACTION_CREATESTAMP: {
                WorkedOnFrame.AddStamp(SavedStamp, ConvertedClick);
                break;
            }
        }
        return true;
    }

    /**
     * Draws editor objects scaled according to the size of the canvas
     * @param canvas
     * @param c
     * @param X
     * @param Y
     */
    public void Draw(Canvas canvas, Context c, int X, int Y) {
        SetFrameSize(X, Y);
        WorkedOnFrame.Draw(canvas, c, ScaleX, ScaleY);
    }

    public int getCurrentBeat() {
        return CurrentFrame;
    }

    public void SetTime(double seconds) {
        if(CurrentFrame != (int)((seconds / TimePerBeat))){
            SetFrame((int)((seconds / TimePerBeat)));
            Updated = true;
        }
    }

    private void SetFrame(int beatNumber) {
        CurrentFrame = beatNumber;
        CurrentTime = TimePerBeat * (double)CurrentFrame;
        WorkedOnFrame = AllFrames.get(CurrentFrame);
    }

    public double GetTime(){
        double time = TimePerBeat * CurrentFrame;
        return time;
    }

    public void GetLine(String line, int i) {
        switch (i) {
            case 1: {//Bullet Stamp
                String[] Parameters = line.split(",");
                int Beat = Integer.parseInt(Parameters[0]);
                int X = Integer.parseInt(Parameters[1]);
                int Y = Integer.parseInt(Parameters[2]);
                String[] Arguments = Arrays.copyOfRange(Parameters, 3, Parameters.length);
                EditorStampTemplate NewTemplate = new EditorStampTemplate(1, Arguments);
                EditorStamp NewStamp = new EditorStamp(Beat, new Point(X, Y), NewTemplate);
                EditorFrame PlacedFrame = AllFrames.get(Beat);
                PlacedFrame.AddStamp(NewStamp);
                break;
            }
            case 2: {//Enemy Stamp
                String[] SplitStamps = line.split(":");
                String[] ParametersForPaired = SplitStamps[1].split(",");
                String[] ParametersForNormal = SplitStamps[0].split(",");
                int Beat = Integer.parseInt(ParametersForNormal[0]);
                int X = Integer.parseInt(ParametersForNormal[1]);
                int Y = Integer.parseInt(ParametersForNormal[2]);
                String[] Arguments = Arrays.copyOfRange(ParametersForNormal, 3, ParametersForNormal.length);
                EditorStampTemplate NewTemplate = new EditorStampTemplate(2, Arguments);
                PairedBullet NewPaired = new PairedBullet(Arrays.copyOfRange(ParametersForPaired, 0, ParametersForPaired.length - 1), true, ParametersForPaired[7]);
                NewTemplate.SetBulletDetails(NewPaired);
                EditorStamp NewStamp = new EditorStamp(Beat, new Point(X, Y), NewTemplate);
                EditorFrame PlacedFrame = AllFrames.get(Beat);
                PlacedFrame.AddStamp(NewStamp);
                break;
            }
            case 3: {//Spawner Stamp
                String[] SplitStamps = line.split(":");
                String[] ParametersForPaired = SplitStamps[1].split(",");
                String[] ParametersForNormal = SplitStamps[0].split(",");
                int Beat = Integer.parseInt(ParametersForNormal[0]);
                int X = Integer.parseInt(ParametersForNormal[1]);
                int Y = Integer.parseInt(ParametersForNormal[2]);
                String[] Arguments = Arrays.copyOfRange(ParametersForNormal, 3, ParametersForNormal.length);
                EditorStampTemplate NewTemplate = new EditorStampTemplate(3, Arguments);
                PairedBullet NewPaired = new PairedBullet(Arrays.copyOfRange(ParametersForPaired, 0, ParametersForPaired.length), false, "");
                NewTemplate.SetBulletDetails(NewPaired);
                EditorStamp NewStamp = new EditorStamp(Beat, new Point(X, Y), NewTemplate);
                EditorFrame PlacedFrame = AllFrames.get(Beat);
                PlacedFrame.AddStamp(NewStamp);
                break;
            }
        }
    }
}

package javaprojects.finalcoursework.beatinvaders.Editor;

/**
 * Parameters for a linked bullet that will be used on enemies and spawners
 */
public class PairedBullet {
    boolean EnemyPair;
    int Type;
    String Speed;
    String Size;
    String Rotation;
    String Extra1,Extra2,Extra3;
    String Homing = "0";

    /**
     * Takes string of details, a flag whether it's for a spawner or enemy and an extra string for it's homing value
     * Homing value is only used for enemies
     * @param Details
     * @param Flag
     * @param Extra
     */
    public PairedBullet(String[] Details, boolean Flag, String Extra){
        EnemyPair = Flag;
        switch(Details[0]){
            case "Normal":{
                Type = 0;
                break;
            }
            case "Circle":{
                Type = 1;
                break;
            }
            case "Flower":{
                Type = 2;
                break;
            }
            case "Spread":{
                Type = 3;
                break;
            }
            default:{
                Type = Integer.parseInt(Details[0]);
                break;
            }
        }
        Speed = Details[1];
        Size = Details[2];
        Rotation = Details[3];
        Extra1 = Details[4];
        Extra2 = Details[5];
        Extra3 = Details[6];
        if(EnemyPair){
            Homing = Extra;
        }
    }

    public String ToString(){
        String Construct = "";
        Construct += ":"+Type;
        if(EnemyPair){
            Construct += ","+Homing;
        }
        Construct+=","+Speed+","+Size+","+Rotation+","+Extra1+","+Extra2+","+Extra3;
        return Construct;
    }
}

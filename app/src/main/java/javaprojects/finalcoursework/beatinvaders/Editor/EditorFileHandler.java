package javaprojects.finalcoursework.beatinvaders.Editor;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javaprojects.finalcoursework.beatinvaders.Song.Song;

/**
 * Library for the editor to save and loads it's frames to and from files
 */
public class EditorFileHandler {

    /**
     * Saves given frames to the files associated with the given song object
     * @param Frames all frames from editor
     * @param SongToBeSaved song that was being edited
     * @param C context
     */
    static public void SaveEditor(ArrayList<EditorFrame> Frames, Song SongToBeSaved, Context C){
        File SaveDir = Environment.getExternalStorageDirectory();
        SaveDir = new File(SaveDir,"Beat Invaders");
        SaveDir = new File(SaveDir, "Songs");
        SaveDir = new File(SaveDir, SongToBeSaved.Title);
        SaveDir = new File(SaveDir, SongToBeSaved.LevelName);
        //Gets to level directory
        try {
            BufferedWriter Bullets, Enemies, Spawners, Events;
            FileWriter BulletsWriter, EnemiesWriter, SpawnerWriter, EventsWriter;
            BulletsWriter = new FileWriter(new File(SaveDir, "BulletsFile.txt"));
            Bullets = new BufferedWriter(BulletsWriter);
            EnemiesWriter = new FileWriter(new File(SaveDir, "EnemiesFile.txt"));
            Enemies = new BufferedWriter(EnemiesWriter);
            SpawnerWriter = new FileWriter(new File(SaveDir, "SpawnerFile.txt"));
            Spawners = new BufferedWriter(SpawnerWriter);
            EventsWriter = new FileWriter(new File(SaveDir, "EventsFile.txt"));
            Events = new BufferedWriter(EventsWriter);
            //Opens all writers and sends references to each frame.
            for(EditorFrame CurrentFrame : Frames){
             CurrentFrame.WriteToFiles(Bullets,Enemies,Spawners,Events);
            }
            Bullets.close();
            Enemies.close();
            Spawners.close();
            Events.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Loads given level attached to the editor by reading from level files
     * @param LevelEditor Editor to load
     * @throws IOException files could not be found
     */
    public static void LoadLevel(Editor LevelEditor) throws IOException {
        File SaveDir = Environment.getExternalStorageDirectory();
        SaveDir = new File(SaveDir,"Beat Invaders");
        SaveDir = new File(SaveDir, "Songs");
        SaveDir = new File(SaveDir, LevelEditor.EditedSong.Title);
        SaveDir = new File(SaveDir, LevelEditor.EditedSong.LevelName);
        BufferedReader Bullets, Enemies, Spawners, Events;
        FileReader BulletsReader, EnemiesReader, SpawnerReader, EventsReader;
        BulletsReader = new FileReader(new File(SaveDir, "BulletsFile.txt"));
        Bullets = new BufferedReader(BulletsReader);
        EnemiesReader = new FileReader(new File(SaveDir, "EnemiesFile.txt"));
        Enemies = new BufferedReader(EnemiesReader);
        SpawnerReader = new FileReader(new File(SaveDir, "SpawnerFile.txt"));
        Spawners = new BufferedReader(SpawnerReader);
        EventsReader = new FileReader(new File(SaveDir, "EventsFile.txt"));
        Events = new BufferedReader(EventsReader);

        String Line;
        while ((Line = Bullets.readLine()) != null)
        {
            LevelEditor.GetLine(Line, 1);
        }
        Bullets.close();
        while ((Line = Enemies.readLine()) != null)
        {
            LevelEditor.GetLine(Line, 2);
        }
        Enemies.close();
        while ((Line = Spawners.readLine()) != null)
        {
            LevelEditor.GetLine(Line, 3);
        }
        Spawners.close();
        Events.close();
    }
}

package javaprojects.finalcoursework.beatinvaders.Menu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javaprojects.finalcoursework.beatinvaders.Button.BitmapButton;
import javaprojects.finalcoursework.beatinvaders.Button.LevelButton;
import javaprojects.finalcoursework.beatinvaders.Button.SongButton;
import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Song.Song;
import javaprojects.finalcoursework.beatinvaders.Song.SongPlayer;

/**
 * Extension of MenuBox that hold all required buttons for the main menu.
 */
public class MainMenu extends MenuBox {

    boolean ButtonFlag = true;
    ArrayList<BitmapButton> CurrentButtons; //Current state buttons
    ArrayList<ArrayList<BitmapButton>> AllButtons; //All normal buttons
    ArrayList<SongButton> SongButtons;
    ArrayList<LevelButton> CurrentLevelButtons;

    static int TYPE_QUIT = -1;

    static int STATE_ROOT = 0;
    static int STATE_EDIT = 1;
    static int STATE_SONGSELECT = 5;
    static int STATE_LEVELSELECT = 6;

    boolean EditorFlag = false;

    SongPlayer PreviewPlayer;

    /**
     * Instantiates objects and menus according to construct provided
     * @param Construct of buttons
     * @param corner1 of entire menu
     * @param corner2 of entire menu
     * @param c
     */
    public MainMenu(String Construct, Point corner1, Point corner2, Context c) {
        super(Construct, corner1, corner2, c);
        PreviewPlayer = new SongPlayer();
    }

    /**
     * Moves all buttons by dx,dy
     * @param dx
     * @param dy
     */
    public void Translate(double dx, double dy) {
        switch(MenuState){
            case 5:{
                for(SongButton BB: SongButtons){
                    BB.Translate(dx,dy);
                }
                break;
            }
            case 6:{
                for(LevelButton BB: CurrentLevelButtons){
                    BB.Translate(dx,dy);
                }
                break;
            }
            default:{
                for(BitmapButton BB: CurrentButtons){
                    BB.Translate(dx,dy);
                }
                break;
            }
        }
    }

    /**
     * Checks whether a button was clicked and reacts accordingly to what type of button/id
     * @param ClickPoint to check
     * @return whether button was clicked
     */
    public boolean CheckClick(Point ClickPoint) {
        if (ClickPoint.X < Corner1.X || ClickPoint.X > Corner2.X || ClickPoint.Y < Corner1.Y || ClickPoint.Y > Corner2.Y) {
            return false;
        } else {
            if (ButtonFlag) {
                for (BitmapButton MB : CurrentButtons) {
                    if (MB.CheckClick(ClickPoint)) {
                        DoButton(MB.getID());
                        return true;
                    }
                }
            } else {
                if (MenuState == STATE_SONGSELECT) {
                    for (SongButton SB : SongButtons) {
                        if (SB.CheckClick(ClickPoint)) {
                            if(SB.GetText().equals("Back")){
                                PreviewPlayer.PauseSong();
                                setState(STATE_ROOT);
                            }else{
                            LoadLevelButtons(SB.GetLevels());
                                try {
                                    PreviewPlayer.SetSong(SB.getSong());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    System.exit(-1);
                                }
                                DoButton(-101);}
                            return true;
                        }
                    }
                } else {
                    for (LevelButton LB : CurrentLevelButtons) {
                        if (LB.CheckClick(ClickPoint)) {
                            if(LB.GetText().equals("Back")){
                                PreviewPlayer.PauseSong();
                                setState(STATE_SONGSELECT);
                            }else {
                                Song Level = LB.GetButtonSong();
                                if(EditorFlag) {
                                    setMessage(13, Level.Title, Level.LevelName);
                                }else{
                                    setMessage(14, Level.Title, Level.LevelName);
                                }
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Sets up Level buttons based on selected song
     * @param AllLevels Levels from song button
     */
    private void LoadLevelButtons(ArrayList<Song> AllLevels) {
        CurrentLevelButtons = new ArrayList<>();

        int ButtonsPerScreen = (int) Height / 300;
        double MenuHeight = (Height / (ButtonsPerScreen + 1.0));
        double GapPerMenu = MenuHeight / ButtonsPerScreen;
        double DistanceBetweenMenus = MenuHeight + GapPerMenu;

        double X1, X2;
        X1 = Width * 0.1;
        X2 = Width * 0.9;

        int ButtonCounter = 0;
        for (Song Level : AllLevels) {
            Point P1 = new Point(X1, 0 + (ButtonCounter * DistanceBetweenMenus));
            Point P2 = new Point(X2, MenuHeight + (ButtonCounter * DistanceBetweenMenus));
            LevelButton CurrentButton = new LevelButton(P1, P2, "button_menu", Level.LevelName, C, Level);
            CurrentLevelButtons.add(CurrentButton);
            ButtonCounter++;
        }

        Point P1 = new Point(X1, 0 + (ButtonCounter * DistanceBetweenMenus));
        Point P2 = new Point(X2, MenuHeight + (ButtonCounter * DistanceBetweenMenus));
        LevelButton BackButton = new LevelButton(P1, P2,  "button_menu", "Back", C, null);
        CurrentLevelButtons.add(BackButton);
    }


    /**
     * Creates buttons based on construct
     * @param Construct
     */
    @Override
    protected void CheckConstruct(String Construct) {
        AllButtons = new ArrayList<>();
        String[] SplitMenus = Construct.split(":");
        int LevelCounter = 0;
        int ButtonsPerScreen = (int) Height / 300;
        double MenuHeight = (Height / (ButtonsPerScreen + 1.0));
        double GapPerMenu = MenuHeight / ButtonsPerScreen;
        double DistanceBetweenMenus = MenuHeight + GapPerMenu;

        double X1, X2;
        X1 = Width * 0.1;
        X2 = Width * 0.9;


        for (String SelectedMenu : SplitMenus) {
            String[] SplitMenu = SelectedMenu.split(",");
            ArrayList<BitmapButton> CurrentMenu = new ArrayList<>();
            int ButtonCounter = 0;
            for (String ButtonText : SplitMenu) {
                Point P1 = new Point(X1, 0 + (ButtonCounter * DistanceBetweenMenus));
                Point P2 = new Point(X2, MenuHeight + (ButtonCounter * DistanceBetweenMenus));
                int TempID = GetID(ButtonText);
                if (TempID == 0) {
                    LevelCounter++;
                    TempID = LevelCounter;
                }
                BitmapButton CurrentButton = new BitmapButton(P1, P2, TempID, "button_menu", ButtonText, C);
                CurrentMenu.add(CurrentButton);
                ButtonCounter++;
            }
            AllButtons.add(CurrentMenu);
            if (AllButtons.size() == 2) {
                break;
            }
        }
        try {
            SetUpSongButtons(SplitMenus[2]);
        } catch (IOException e) {
            Log.d("Song Setup Menu", "Error settings up song buttons");
            e.printStackTrace();
        }
    }

    /**
     * Sets up separate song buttons based on what songs exist in directory
     * @param SongMenu to use
     * @throws IOException if song could not be found
     */
    private void SetUpSongButtons(String SongMenu) throws IOException {
        SongButtons = new ArrayList<>();
        int ButtonsPerScreen = (int) Height / 300;
        double MenuHeight = (Height / (ButtonsPerScreen + 1.0));
        double GapPerMenu = MenuHeight / ButtonsPerScreen;
        double DistanceBetweenMenus = MenuHeight + GapPerMenu;

        double X1, X2;
        X1 = Width * 0.1;
        X2 = Width * 0.9;

        String[] Songs = SongMenu.split(",");
        int ButtonCounter = 0;
        for (String ButtonSong : Songs) {
            Point P1 = new Point(X1, 0 + (ButtonCounter * DistanceBetweenMenus));
            Point P2 = new Point(X2, MenuHeight + (ButtonCounter * DistanceBetweenMenus));

            SongButton CurrentButton = new SongButton(P1, P2, -101, "button_menu", ButtonSong, C, ButtonSong);
            SongButtons.add(CurrentButton);
            ButtonCounter++;
        }
    }

    /**
     * Gets fixed id for the given text
     * @param ButtonText
     * @return
     */
    private int GetID(String ButtonText) {
        switch (ButtonText) {
            case "Back": {
                return -1;
            }
            case "Quit": {
                return -999;
            }
            case "Levels": {
                return -6;
            }
            case "New Level": {
                return -61;
            }
            case "Existing Level": {
                return -62;
            }
            case "Scores": {
                return -7;
            }
            case "Editor": {
                return -2;
            }
            case "Options": {
                return -3;
            }
            case "Volume Settings": {
                return -4;
            }
            case "Music Volume": {
                return -41;
            }
            case "Effects Volume": {
                return -42;
            }
            case "Song Settings": {
                return -5;
            }
            case "Global Delay": {
                return -51;
            }
            case "PlaceHolder": {
                return -52;
            }
            default: {
                return 0;
            }
        }
    }

    /**
     * Does different actions based on ID provided
     * @param id
     */
    @Override
    protected void DoButton(int id) {
        PreviewPlayer.PauseSong();
        setMessage(0,"", "");
        switch (id) {
            case -6: {
                setState(STATE_SONGSELECT);
                break;
            }
            case -999: {
                setMessage(TYPE_QUIT, "", "");
                break;
            }
            case -1: {
                EditorFlag = false;
                switch (MenuState) {
                    case 1: {
                        setState(STATE_ROOT);
                        break;
                    }
                    case 2: {
                        setState(STATE_ROOT);
                        break;
                    }
                    case 5: {
                        setState(STATE_ROOT);
                        break;
                    }
                    case 6: {
                        setState(STATE_SONGSELECT);
                        break;
                    }
                }
                break;
            }
            case -2: {
                setState(STATE_EDIT);

                break;
            }
            case -61: { //New Song
                setMessage(12, "", "");
                break;
            }
            case -62: {
                setState(STATE_SONGSELECT);
                EditorFlag = true;
                break;
            }
            case -101: {
                setState(STATE_LEVELSELECT);
                PreviewPlayer.PlaySong();
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * Sends messages to the handler based on what buttons are pressed
     * @param Type of message
     * @param SongName to play/edit
     * @param LevelName to play/edit
     */
    private void setMessage(int Type, String SongName, String LevelName) {
        if(Type != 0){
            PreviewPlayer.Delete();
        }
        MenuMessage = new Message();
        MenuMessage.what = Type;
        Bundle B = new Bundle();
        B.putString("SongName", SongName);
        B.putString("LevelName", LevelName);
        MenuMessage.setData(B);
    }

    /**
     * Draws all buttons for selected state
     * @param canvas
     */
    @Override
    public void DrawMenu(Canvas canvas) {
        Paint Test = new Paint();
        Test.setColor(Color.BLACK);
        RectF Bounds = new RectF(0, 0, (float) Width, (float) Height);
        canvas.drawRect(Bounds, Test);
        if (ButtonFlag) {
            for (BitmapButton BB : CurrentButtons) {
                BB.DrawButton(canvas);
            }
        } else {
            if (MenuState == STATE_SONGSELECT) {
                for (SongButton SB : SongButtons) {
                    SB.DrawButton(canvas);
                }
            } else {
                for (LevelButton LB : CurrentLevelButtons) {
                    LB.DrawButton(canvas);
                }
            }
        }
    }

    /**
     * Changes state and sets buttons
     * @param state
     */
    public void setState(int state) {
        MenuState = state;
        switch (state) {
            case 0: {
                CurrentButtons = AllButtons.get(0);
                PreviewPlayer.PauseSong();
                ButtonFlag = true;
                break;
            }
            case 1: {
                CurrentButtons = AllButtons.get(1);
                PreviewPlayer.PauseSong();
                ButtonFlag = true;
                break;
            }
            default: {
                ButtonFlag = false;
            }
        }
    }

}

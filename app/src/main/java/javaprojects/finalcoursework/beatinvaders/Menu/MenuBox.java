package javaprojects.finalcoursework.beatinvaders.Menu;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Message;

import javaprojects.finalcoursework.beatinvaders.Button.Button;
import javaprojects.finalcoursework.beatinvaders.Extras.Point;

import java.util.ArrayList;

/**
 * Abstract menu class that holds base functions
 */
public abstract class MenuBox {
    Point Corner1, Corner2;
    double Width, Height;

    int MenuState;

    Message MenuMessage;

    Context C;

    /**
     * Takes points and string to turn into buttons
     * @param Construct
     * @param corner1
     * @param corner2
     * @param c
     */
    public MenuBox(String Construct, Point corner1, Point corner2, Context c){
        Corner1 = corner1;
        Corner2 = corner2;
        C = c;
        Width = Corner2.X - Corner1.X;
        Height = Corner2.Y - Corner1.Y;
        CheckConstruct(Construct);
        MenuState = 0;
    }

    protected abstract void CheckConstruct(String Construct);

    public abstract boolean CheckClick(Point ClickPoint);

    public abstract void Translate(double dx, double dy);

    protected abstract void DoButton(int id);

    protected abstract void DrawMenu(Canvas canvas);

    public Message getMessage(){
        return MenuMessage;
    }
}

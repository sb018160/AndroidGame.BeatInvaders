package javaprojects.finalcoursework.beatinvaders;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import javaprojects.finalcoursework.beatinvaders.Editor.EditorActivity;
import javaprojects.finalcoursework.beatinvaders.Game.GameActivity;
import javaprojects.finalcoursework.beatinvaders.MenuActivity.MenuActivity;
import javaprojects.finalcoursework.beatinvaders.Song.SongSetupActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CheckPermissions();
    }

    /**
     * Runs first time setup then starts the menu activity
     */
    private void Start() {
        FirstTimeSetup();
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        Intent I = new Intent();
        I.putExtra("What", 0);
        SetActivity(1, I);
    }

    /**
     * Checks result of activity and either closes application or opens a new one
     * @param requestCode
     * @param resultCode
     * @param data
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("REQUEST CODE", "CODE = " + requestCode);
        if (resultCode == -1) {
            Log.d("DEBUG", "ACTIVITY KILLED");
            finish();
        } else {
            SetActivity(resultCode, data);
        }
    }

    /**
     * Sets activity to be opened based on ID and the data passed from prior activity
     * @param ID to open
     * @param data from old activity
     */
    private void SetActivity(int ID, Intent data) {
        Intent intent;
        switch (ID) {
            case 1: { //Main Menu
                intent = new Intent(this, MenuActivity.class);
                break;
            }
            case 2: {
                intent = new Intent(this, SongSetupActivity.class);
                break;
            }
            case 3: {
                intent = new Intent(this, EditorActivity.class);
                break;
            }
            case 4: {
                intent = new Intent(this, GameActivity.class);
                break;
            }
            default: {
                intent = new Intent(this, MenuActivity.class);
                break;
            }
        }

        switch (data.getIntExtra("What", 0)) {
            case 0: {//Fresh Start;
                break;
            }
            case 12: {//Going from Menu to SongSetup
                break;
            }
            case 23: {//Going from SongSetup to Editor
                Bundle Data = data.getBundleExtra("Data");
                intent.putExtra("Data", Data);
                intent.putExtra("Flag", false);
                break;
            }
            case 13: {//Going from menu to editor with existing song
                intent.putExtra("SongName", data.getStringExtra("SongName"));
                intent.putExtra("LevelName", data.getStringExtra("LevelName"));
                intent.putExtra("Flag", true);
                break;
            }
            case 14: {//Going from Menu to Level
                intent.putExtra("SongName", data.getStringExtra("SongName"));
                intent.putExtra("LevelName", data.getStringExtra("LevelName"));
                intent.putExtra("Flag", false);
                break;
            }
            case 34: {//Going from editor to level
                intent.putExtra("SongName", data.getStringExtra("SongName"));
                intent.putExtra("LevelName", data.getStringExtra("LevelName"));
                intent.putExtra("Flag", true);
                break;
            }
            case 21: {//Going from songsetup back to menu

                break;
            }
            case 31: {//Going from Editor back to menu

                break;
            }
            case 43://Level to Editor
            {
                intent.putExtra("SongName", data.getStringExtra("SongName"));
                intent.putExtra("LevelName", data.getStringExtra("LevelName"));
                intent.putExtra("Flag", true);
                break;
            }
            case 41://Level to Menu
            {
                break;
            }
        }
        this.startActivityForResult(intent, 0);
    }

    /**
     * Checks whether game files exist and creates if not
     */
    private void FirstTimeSetup() {
        File NewGameDir = Environment.getExternalStorageDirectory();
        NewGameDir = new File(NewGameDir, "Beat Invaders");
        if (NewGameDir.exists()) {
            return;
        } else {
            NewGameDir.mkdirs();
            NewGameDir = new File(NewGameDir, "Songs");
            NewGameDir.mkdirs();
        }
        try {
            copyAssets();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Copies over existing levels from assets into the game directory
     * @throws IOException
     */
    private void copyAssets() throws IOException {
        AssetManager assetManager = getAssets();
        File GameDir = Environment.getExternalStorageDirectory();
        GameDir = new File(GameDir, "Beat Invaders");
        GameDir = new File(GameDir, "Songs");
        String[] SongSubdirectories = assetManager.list("Songs");
        String[] SongFiles = {"BulletsFile.txt", "EventsFile.txt", "EnemiesFile.txt", "SpawnerFile.txt", "SongInfo.txt", ""};
        for (String SongName : Objects.requireNonNull(SongSubdirectories)) { //For number of songs in folder
            File SongDir = new File(GameDir, SongName);
            SongDir.mkdir(); //Create new directory for song
            SongFiles[5] = SongName + ".mp3"; //Set 4th to song name.mp3
            String[] LevelSubdirectories = assetManager.list("Songs/" + SongName);
            for (String Level : Objects.requireNonNull(LevelSubdirectories)) {
                File LevelDirectory = new File(SongDir, Level);
                LevelDirectory.mkdirs();
                for (String FileName : SongFiles) { //For number of files
                    File FileDir = new File(LevelDirectory, FileName);
                    InputStream istr = assetManager.open("Songs/" + SongName + "/" + Level + "/" + FileName);
                    OutputStream ostr = new FileOutputStream(FileDir);
                    CopyStreams(istr, ostr); //Copy streams
                }
            }
        }
    }

    /**
     * Used for copying mp3 files
     * @param istr
     * @param ostr
     * @throws IOException
     */
    private void CopyStreams(InputStream istr, OutputStream ostr) throws IOException {
        byte[] buffer = new byte[1024];
        int length;
        while ((length = istr.read(buffer)) > 0) {
            ostr.write(buffer, 0, length);
        }
        ostr.flush();
        ostr.close();
        istr.close();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /**
     * Checks whether app has been granted required permissions, these are needed to play the app as read is needed to go to external directory and write is needed to write to that directory
     */
    private void CheckPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Start();
        } else {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);

        }
    }

    /**
     * Starts the app once permissions are granted if they were not granted in the first place.
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Start();
            } else {
                System.exit(0);
            }
        }
    }
}


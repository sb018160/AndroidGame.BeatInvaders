package javaprojects.finalcoursework.beatinvaders.Button;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Song.Song;

/**
 * Extention of Bitmap button to hold list of levels for given song
 */
public class SongButton extends BitmapButton {

    ArrayList<Song> Levels;

    /**
     * Takes in a song name and checks for levels
     * @param corner1
     * @param corner2
     * @param id
     * @param BitmapName
     * @param Text
     * @param C
     * @param SongName
     * @throws IOException
     */
    public SongButton(Point corner1, Point corner2, int id, String BitmapName, String Text, Context C, String SongName) throws IOException {
        super(corner1, corner2, id, BitmapName, Text, C);
        if (!Text.equals("Back")) {
            GetSongLevels(SongName);
        }
    }

    public String GetText() {
        return ButtonText;
    }

    /**
     * Gets levels for song by checking the song directory
     * @param SongName
     * @throws IOException
     */
    private void GetSongLevels(String SongName) throws IOException {
        Levels = new ArrayList<>();
        File SongDir = Environment.getExternalStorageDirectory();
        SongDir = new File(SongDir, "Beat Invaders");
        SongDir = new File(SongDir, "Songs");
        SongDir = new File(SongDir, SongName);
        //Gets to song directory
        String[] LevelNames = SongDir.list();
        //Lists all directories within the song directory, these should all be levels
        for (String LevelName : LevelNames) {
            File LevelDir = new File(SongDir, LevelName);
            FileReader fileReader = new FileReader(new File(LevelDir, "SongInfo.txt"));
            //Gets the song info per level and saves the details
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            Song LevelSong = new Song(bufferedReader.readLine());
            bufferedReader.close();
            Levels.add(LevelSong);
        }
    }

    public ArrayList<Song> GetLevels() {
        return Levels;
    }

    public Song getSong() {
        return(Levels.get(0));
    }
}

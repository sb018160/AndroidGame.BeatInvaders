package javaprojects.finalcoursework.beatinvaders.Button;

import android.content.Context;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;

/**
 * Abstract Button Class which defines a rectangle with an ID,
 * Translate and check if Point is within boundary function.
 */
public abstract class Button {
    protected int ID;
    protected Point Corner1, Corner2;
    protected double Width, Height;

    Context C;

    /**
     * Button constructor,
     * Context required for drawing self from an image
     * @param P1 Bottom left corner
     * @param P2 Top right corner
     * @param id to be set to
     * @param c Context
     */
    public Button(Point P1, Point P2, int id, Context c){
        Corner1 = P1;
        Corner2 = P2;
        ID = id;
        C = c;
        Width = Corner2.X - Corner1.X;
        Height = Corner2.Y - Corner1.Y;
    }

    /**
     * Checks if a given point is within the button rectangle.
     * Returns true if so, used for checking clicks
     * @param ClickLocation
     * @return Whether point is inside rectangle
     */
    public boolean CheckClick(Point ClickLocation){
        if(Corner1.X <= ClickLocation.X && ClickLocation.X <= Corner2.X && Corner1.Y <= ClickLocation.Y && ClickLocation.Y <= Corner2.Y){
            return true;
        }
        return false;
    }

    /**
     * Translates the entire button by given values
     * @param XOffset
     * @param YOffset
     */
    public void Translate(double XOffset, double YOffset) {
        Corner1.X += XOffset;
        Corner1.Y += YOffset;
        Corner2.X += XOffset;
        Corner2.Y += YOffset;
    }

    public int getID() {
        return ID;
    }


}

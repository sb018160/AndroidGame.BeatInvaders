package javaprojects.finalcoursework.beatinvaders.Button;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Song.Song;

/**
 * Extension of button which implements drawing self as a bitmap.
 */
public class BitmapButton extends Button {

    protected int ButtonState;
    protected Bitmap ButtonBitmaps;
    protected String ButtonText;

    /**
     * Takes in name of bitmap to get from assets folder and text to be displayed in the centre of button
     * @param corner1
     * @param corner2
     * @param id
     * @param BitmapName Name of bitmap to get from Assets/Graphics
     * @param Text Text to be displayed in button
     * @param C
     */
    public BitmapButton(Point corner1, Point corner2, int id, String BitmapName, String Text, Context C) {
        super(corner1, corner2, id, C);
        ButtonText = Text;
        LoadBitmaps(BitmapName, C);
        ButtonState = 0;
    }

    /**
     * Gets bitmap from asset manager and sets to own bitmap
     * @param bitmapName
     * @param C
     */
    private void LoadBitmaps(String bitmapName, Context C) {
        AssetManager Amanager = C.getAssets();
        try {
            InputStream ImageStream = Amanager.open("Graphics/" + bitmapName + ".png");
            ButtonBitmaps = BitmapFactory.decodeStream(ImageStream);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Draws button on the canvas along with text in the centre of button.
     * @param C
     */
    public void DrawButton(Canvas C) {
        Paint P = new Paint();
        P.setColor(Color.WHITE);
        P.setTextSize(40);
        P.setTextAlign(Paint.Align.CENTER);
        RectF Bounds = new RectF((float) Corner1.X, (float) Corner1.Y, (float) Corner2.X, (float) Corner2.Y);
        C.drawBitmap(ButtonBitmaps, null, Bounds, null);
        C.drawText(ButtonText, (float) (Corner1.X + Width / 2), (float) (Corner1.Y + Height / 2), P);
    }
}

package javaprojects.finalcoursework.beatinvaders.Button;

import android.content.Context;
import android.support.annotation.Nullable;

import javaprojects.finalcoursework.beatinvaders.Extras.Point;
import javaprojects.finalcoursework.beatinvaders.Song.Song;

/**
 * Extension of bitmap button to hold a level within object
 */
public class LevelButton extends BitmapButton {

    Song ButtonSong;

    /**
     * Takes in a level file which is null in the case of a "Back" button
     * @param corner1
     * @param corner2
     * @param BitmapName
     * @param Text
     * @param C
     * @param LevelFile
     */
    public LevelButton(Point corner1, Point corner2, String BitmapName, String Text, Context C,@Nullable Song LevelFile) {
        super(corner1, corner2, 0, BitmapName, Text, C);
        if(!Text.equals("Back")) {
            ButtonSong = LevelFile;
        }
    }

    public String GetText() {
    return ButtonText;
    }

    public Song GetButtonSong(){
        return ButtonSong;
    }
}
